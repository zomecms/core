/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
module.exports = function (grunt) {
// Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        zip: {
            'versioned': {
                cwd: 'dist/<%= pkg.version %>',
                src: 'dist/<%= pkg.version %>/**',
                dest: 'dist/zome_<%= pkg.version %>.zip'
            },
            'latest': {
                cwd: 'dist/<%= pkg.version %>',
                src: 'dist/<%= pkg.version %>/**',
                dest: 'dist/zome_latest.zip'
            }
        },
        copy: {
            main: {
                files: [
                    // includes files within path
                    {expand: true, src: [
                            'iddi.php',
                            'iddi_core.php',
                            'iddi_api.php',
                            'date-functions.lib.php',
                            'mysql.class.php',
                            'no-pic.jpg',
                            'composer.json',
                            '.htaccess',
                            'core/**',
                            'fonts/**',
                            'languages/**',
                            'templates/**'], dest: 'dist/<%= pkg.version %>'},
                ],
            },
        },
        concat: {
            debug: {
                files: {
                    //Create the login app
                    //Create the main app
                    'iddiedit.js': [
                        'scripts/init.js',
                        'scripts/lib/*.js',
                        'scripts/core/*.js',
                        'scripts/ui/*.js',
                        'scripts/editors/*.js',
                        'scripts/admin/*.js',
                        'scripts/dashboard/*.js',
                        'scripts/language/*.js',
                        'scripts/start.js'
                    ],
                    //'dist/<%= pkg.version %>/iddi3.html': ['ui/*.html']
                }
            }
        },
        uglify: {
            options: {
                // the banner is inserted at the top of the output
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
                    'dist/<%= pkg.version %>/iddiedit.js': ['iddiedit.js']
                }
            }
        },
        sass: {
            debug: {
                options: {
                    sourcemap: 'auto',
                    style: 'expanded'
                },
                files: {
                    'iddi.css': 'scss/main.scss'
                }
            },
            dist: {
                options: {
                    sourcemap: 'none',
                    style: 'compressed'
                },
                files: {
                    'dist/<%= pkg.version %>/iddi.css': 'scss/main.scss'
                }
            }
        },
        jshint: {
            beforeconcat: ['scripts/admin/*.js', 'scripts/core/*.js', 'scripts/editors/*.js', 'scripts/ui/*.js'],
            afterconcat: ['dist/iddi3.js']
        },
        jscs: {
            src: ['scripts/admin/*.js', 'scripts/core/*.js', 'scripts/editors/*.js', 'scripts/ui/*.js'],
            options: {
                config: true,
                esnext: true, // If you use ES6 http://jscs.info/overview.html#esnext
                verbose: true, // If you need output with rule names http://jscs.info/overview.html#verbose                
                requireCurlyBraces: ["if", "else", "for", "while", "do"],
                requireSpaceAfterKeywords: ["if", "else", "for", "while", "do", "switch", "return"],
                "requireMultipleVarDecl": true,
                "requireSpaceBeforeBinaryOperators": ["?", "-", "/", "*", "=", "==", "===", "!=", "!==", ">", ">=", "<", "<="],
                "requireSpaceAfterBinaryOperators": ["?", "/", "*", ":", "=", "==", "===", "!=", "!==", ">", ">=", "<", "<="],
                "requireSpaceBeforeBinaryOperators": ["+", "-", "/", "*", "=", "==", "===", "!=", "!=="],
                        "disallowSpaceAfterPrefixUnaryOperators": ["++", "--", "+", "-"],
                "disallowSpaceBeforePostfixUnaryOperators": ["++", "--"],
                "disallowKeywords": ["with"],
                "disallowMultipleLineBreaks": true,
                "disallowKeywordsOnNewLine": ["else"],
                "requireLineFeedAtFileEnd": true,
                "disallowSpaceAfterObjectKeys": true
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-jscs');
    //grunt.loadNpmTasks('grunt-contrib-qunit');
    //grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-zip');
    grunt.registerTask('default', ['jshint', 'concat', 'sass', 'copy', 'uglify', 'zip']);
};
