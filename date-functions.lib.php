﻿<?php
/* 
 * Date functions for other languages
 */
function date_lang($format,$date,$language=null){
  if($language===null && iddiRequest::$current->renderlanguage) $language=iddiRequest::$current->renderlanguage;
  if($language===null) $language=iddiRequest::$current->language;
  $date_function='date_'.$language;
  if(function_exists($date_function)){
    return $date_function($format,$date);
  }else{
    return date($format,$date);
  }
}

function date_ja($format,$date){

  if($format!='M') $format='Y/m/d';

  $replacements=array('January'=>'1月','February'=>'2月','March'=>'3月','April'=>'4月','May'=>'5月','June'=>'6月','July'=>'7月',
                'August'=>'8月','September'=>'9月','October'=>'10月','November'=>'11月','December'=>'12月',
                'Jan'=>'1月','Feb'=>'2月','Mar'=>'3月','Apr'=>'4月','May'=>'5月','Jun'=>'6月','Jul'=>'7月',
                'Aug'=>'8月','Sep'=>'9月','Oct'=>'10月','Nov'=>'11月','Dec'=>'12月',
                'Monday'=>'月曜日','Tuesday'=>'火曜日','Wednesday'=>'水曜日','Thursday'=>'木曜日','Friday'=>'金曜日','Saturday'=>'土曜日','Sunday'=>'日曜日',
                'Mon'=>'月曜日','Tue'=>'火曜日','Wed'=>'水曜日','Thur'=>'木曜日','Fri'=>'金曜日','Sat'=>'土曜日','Sun'=>'日曜日');

  $d=date($format,$date);
  foreach($replacements as $k=>$v) $d=str_replace($k,$v,$d);

  return $d;
}

function date_fr($format,$date)
{
  $replacements=array('January'=>'Janvier','February'=>'Février','March'=>'Mars','April'=>'Avril','May'=>'Mai','June'=>'Juin','July'=>'Juillet',
                'August'=>'Août','September'=>'Septembre','October'=>'Octobre','November'=>'Novembre','December'=>'Décembre',
				'Feb'=>'Fév','Apr'=>'Avr','Jun'=>'Jui','Jul'=>'Jui','Aug'=>'Aoû','Dec'=>'Déc',
                'Monday'=>'Lundi','Tuesday'=>'Mardi','Wednesday'=>'Mercredi','Thursday'=>'Jeudi','Friday'=>'Vendredi','Saturday'=>'Samedi','Sunday'=>'Dimanche',
                'Mon'=>'Lun','Tue'=>'Mar','Wed'=>'Mer','Thur'=>'Jeu','Fri'=>'Ven','Sat'=>'Sam','Sun'=>'Dim');
  $d=date($format,$date);
  foreach($replacements as $k=>$v) $d=str_replace($k,$v,$d);
  return $d;
}

function date_es($format,$date)
{
  $replacements=array('January'=>'Enero','February'=>'Febrero','March'=>'Marzo','April'=>'Abril','May'=>'Mayo','June'=>'Junio','July'=>'Julio',
                'August'=>'Agosto','September'=>'Septiembre','October'=>'Octubre','November'=>'Noviembre','December'=>'Diciembre',
				'Jan'=>'Ene','Apr'=>'Abr','Aug'=>'Ago','December'=>'Dic',
                'Monday'=>'Lunes','Tuesday'=>'Martes','Wednesday'=>'Miércoles','Thursday'=>'Jueves','Friday'=>'Viernes','Saturday'=>'Sábado','Sunday'=>'Domingo',
                'Mon'=>'Lun','Tue'=>'Mar','Wed'=>'Mié','Thur'=>'Jue','Fri'=>'Vie','Sat'=>'Sáb','Sun'=>'Dom');
  $d=date($format,$date);
  foreach($replacements as $k=>$v) $d=str_replace($k,$v,$d);
  return $d;
}

function date_pt($format,$date)
{		
  $replacements=array('January'=>'Janeiro','February'=>'Fevereiro','March'=>'Março','April'=>'Abril','May'=>'Maio','June'=>'Junho','July'=>'Julho',
                'August'=>'Agosto','September'=>'Setembro','October'=>'Outubro','November'=>'Novembro','December'=>'Dezembro',
				'Feb'=>'Fev','Apr'=>'Abr','May'=>'Mai','Aug'=>'Ago','Sep'=>'Set','Oct'=>'Out','Dec'=>'Dez',
                'Monday'=>'Segunda-feira','Tuesday'=>'Terça-feira','Wednesday'=>'Quarta-feira','Thursday'=>'Quinta-feira','Friday'=>'Sexta-feira','Saturday'=>'Sábado','Sunday'=>'Domingo',
                'Mon'=>'Seg','Tue'=>'Ter','Wed'=>'Qua','Thur'=>'Qui','Fri'=>'Sex','Sat'=>'Sáb','Sun'=>'Dom');
  $d=date($format,$date);
  foreach($replacements as $k=>$v) $d=str_replace($k,$v,$d);
  return $d;
}

function date_de($format,$date)
{		
  $replacements=array('January'=>'Januar','February'=>'Februar','March'=>'März','May'=>'Mai','June'=>'Juni','July'=>'Juli',
                'October'=>'Oktober','December'=>'Dezember',
				'Mar'=>'Mär','May'=>'Mai','Oct'=>'Okt','Dec'=>'Dez',
                'Monday'=>'Montag','Tuesday'=>'Dienstag','Wednesday'=>'Mittwoch','Thursday'=>'Donnerstag','Friday'=>'Freitag','Saturday'=>'Samstag','Sunday'=>'Sonntag',
                'Mon'=>'Mo','Tue'=>'Di','Wed'=>'Mi','Thur'=>'Do','Fri'=>'Fr','Sat'=>'Ss','Sun'=>'So');
  $d=date($format,$date);
  foreach($replacements as $k=>$v) $d=str_replace($k,$v,$d);
  return $d;
}