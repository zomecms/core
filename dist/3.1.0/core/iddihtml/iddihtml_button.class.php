<?php
/**
 * iddiHtml_Input class.
 *
 * Just sets input tags as void and self closing (depending on doctype)
 *
 * @author J.Patchett
 * @package IDDI Core
 * */
class iddiHtml_Button extends iddiXmlNode {   
    function __construct() {
        $this->setnodename('button');
    }
}
