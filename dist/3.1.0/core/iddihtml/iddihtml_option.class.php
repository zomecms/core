<?php
/**
 * iddiHtml_Img class.
 *
 * Sets img tags as void and self closing (depending on doctype) and also ensures
 * that they have an alt tag so that it validates
 *
 * @author J.Patchett
 * @package IDDI Core
 * */
class iddiHtml_Option extends iddiXmlNode {
    
    static function build($id,$value,$class=''){
        $new = new iddiHtml_Option();
        $new->attributes['ID']=$id;
        $new->attributes['CLASS']=$id;
        $new->setValue($value);
        return $new;
    }
    
    function __construct() {
        $this->setnodename('option');
    }
}
