<?php
/**
 * iddiHtml_Input class.
 *
 * Just sets input tags as void and self closing (depending on doctype)
 *
 * @author J.Patchett
 * @package IDDI Core
 * */
class iddiHtml_Input extends iddiHtml_Void {
    
    static function build_radio($id,$name,$value){
        $new = new iddiHtml_Input();
        $new->attributes['TYPE']='radio';
        $new->attributes['ID']=$id;
        $new->attributes['NAME']=$name;
        $new->attributes['VALUE']=$value;
        return $new;
    }
    
    static function build_checkbox($id,$name,$value){
        $new = new iddiHtml_Input();
        $new->attributes['TYPE']='checkbox';
        $new->attributes['ID']=$id;
        $new->attributes['NAME']=$name;
        $new->attributes['VALUE']=$value;
        return $new;
    }    
    
    function __construct() {
        $this->setnodename('input');
    }
}
