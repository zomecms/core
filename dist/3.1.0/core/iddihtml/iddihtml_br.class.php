<?php

/**
 * iddiHtml_Br class.
 *
 * Just sets br tags as void and self closing (depending on doctype)
 *
 * @author J.Patchett
 * @package IDDI Core
 * */
class iddiHtml_Br extends iddiXmlNode {
    var $is_void_html_tag = false;
    var $is_self_closing_tag = true;
}
