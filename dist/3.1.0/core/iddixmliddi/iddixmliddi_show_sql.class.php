<?php
    /**
    * iddiXmlIddi_Show_Sql Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Show_Sql extends iddiXmlIddiNode{
      function output(){
        $this->preparse();
        //Get the next query builder node
        $qp=$this->getParentOfType('iddiXmlIddi_Query_Parser');
        return '<pre>'.$qp->sql.'</pre>';
      }
    }

   