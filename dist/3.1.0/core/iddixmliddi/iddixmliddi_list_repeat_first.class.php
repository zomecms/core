<?php
    /**
    * iddiXmlIddi_List_Repeat_First Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_List_Repeat_First extends iddiXmlIddiNode{
        function preparse(){
            $qp=$this->getParentOfType('iddiXmlIddi_Query_Parser');
            $qp->repeatable_first=$this;
        }
    }
   