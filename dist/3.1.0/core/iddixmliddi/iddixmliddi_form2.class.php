<?php

/**
 * @example
 * <iddi:form method="post" action="" entity="project" modeswitch="action" allow_repost="false" errorclass="error">
 *  <!-- Heading -->
 *  <iddi:form-mode modes="create"><h2>Create Project</h2></iddi:form-mode>
 *  <iddi:form-mode modes="edit"><h2>Edit Project <iddi:value-of select="project_name"/></h2></iddi:form-mode>
 *  <iddi:form-mode modes="delete"><h2>Delete Project <iddi:value-of select="project_name"/></h2></iddi:form-mode>
 *
 *  <!-- Define / Override form templates -->
 *  <iddi:input-template type="text">
 *   <div class="text_field{field_error_class}" id="{field_name}">
 *    <label>
 *     <iddi:if-form-error name="{field_name}"><p><iddi:form-error/></p></iddi:if-form-error>
 *     <span><iddi:value-of select="caption"> : </span>
 *     <input type="text" name="{field_name}" value="{field_value}"/>
 *    </label>
 *  </iddi:input-template>
 *
 *  <!-- Notices -->
 *  <iddi:form-mode modes="delete_success"><p>Deleted succesfully</p></iddi:form-mode>
 *  <iddi:form-mode modes="delete_fail"><p>Delete failed</p></iddi:form-mode>
 *  <iddi:form-mode modes="create_success"><p>Succesfully created</p></iddi:form-mode>
 *  <iddi:form-mode modes="create_fail"><p>Failed to create <iddi:if-form-error> - scroll down for errors</iddi:if-form-error></p></iddi:form-mode>
 *  <iddi:form-mode modes="save_success"><p>Succesfully saved</p></iddi:form-mode>
 *  <iddi:form-mode modes="save_fail"><p>Failed to save <iddi:if-form-error> - scroll down for errors</iddi:if-form-error></p></iddi:form-mode>
 *
 *  <!-- Edit / Create Form -->
 *  <iddi:form-mode modes="edit create">
 *   <iddi:form-input name="Project Name" type="text" maxlength="50" minlength="3"/>
 *   <iddi:form-input name="Customer" type="lookup" lookup="customer" required="true"/>
 *   <iddi:form-mode modes="create">
 *    <!-- Ref code can only be set when being created -->
 *    <iddi:form-input name="Ref Code" type="text" maxlength="5" minlength="5"/>
 *   </iddi:form-mode>
 *   <iddi:form-mode modes="edit">
 *    <!-- Ref code can only be created once display only when in edit mode -->
 *    <div class="text_field"><label><span>Ref Code : </span><p><iddi:value-of select="ref_code"/></p><label></div>
 *   </iddi:form-mode>
 *
 *   <!-- Submit buttons for edit/create mode -->
 *   <fieldset class="buttons">
 *    <iddi:form-mode modes="create">
 *     <iddi:form-input type="submit" value="create" name="action" text="Create Project"/>
 *    </iddi:form-mode>
 *    <iddi:form-mode modes="edit">
 *     <iddi:form-input type="submit" value="save" name="action" text="Save Project"/>
 *    </iddi:form-mode>
 *   </fieldset>
 *  </iddi:form-mode>
 *
 *  <!-- Delete mode -->
 *  <iddi:form-mode modes="delete">
 *   <fieldset class="buttons">
 *     <p>Are you sure you want to delete this project?</p>
 *     <iddi:form-input type="submit" value="delete_cancel" name="action" text="No"/>
 *     <iddi:form-input type="submit" value="delete_confirm" name="action" text="Yes"/>
 *   </fieldset>
 *  </iddi:form-mode>
 * </iddi:form>
 *
 * Built in modes are : create,create_success,create_fail,edit,edit_sucess,edit_fail,delete,delete_success,delete_fail
 * Built in types are : text,lookup,number,date,textarea,yesno,checkbox,radio,file,image
 *
 * Shown above are some defaults. Shortest possible version of the above is:
 *
 * <iddi:form entity="project">
 *
 *  <!-- Edit / Create Form -->
 *  <iddi:form-mode modes="edit create">
 *   <iddi:form-input name="Project Name" type="text" maxlength="50" minlength="3"/>
 *   <iddi:form-input name="Customer" type="lookup" lookup="customer" required="true"/>
 *   <iddi:form-mode modes="create">
 *    <!-- Ref code can only be set when being created -->
 *    <iddi:form-input name="Ref Code" type="text" maxlength="5" minlength="5"/>
 *   </iddi:form-mode>
 *   <iddi:form-mode modes="edit">
 *    <!-- Ref code can only be created once display only when in edit mode -->
 *    <div class="text_field"><label><span>Ref Code : </span><p><iddi:value-of select="ref_code"/></p><label></div>
 *   </iddi:form-mode>
 *
 *   <!-- Submit buttons for edit/create mode -->
 *   <fieldset class="buttons">
 *    <iddi:form-buttons/>
 *   </fieldset>
 *  </iddi:form-mode>
 *
 *  <!-- Delete mode -->
 *  <iddi:form-mode modes="delete">
 *   <fieldset class="buttons">
 *    <iddi:form-buttons/>
 *   </fieldset>
 *  </iddi:form-mode>
 * </iddi:form>
 *
 */


    class iddiXmlIddi_Form2 extends iddiEntity {
        function parse2(){
            $this->_e=$this;
            $entity_name=$this->attributes['ENTITY'];
            $id_attr=$this->attributes['IDID'];
            $action_attr=$this->attributes['ACTIONID'];
            if($id_attr=='') $id_attr='id';
            if($action_attr=='') $action_attr='action';

            $action=($_GET[$action_attr])?$_GET[$action_attr]:$_POST[$action_attr];
            $id=$_GET[$id_attr];

            if($id==0) $this->set_mode('create');
            if($id>0) $this->set_mode('edit');


            $this->entityname=$entity_name;
            if($id>0){
                $this->loadById($entity_name,$id);
            }else{
                if($this->virtualfilename=='') $this->virtualfilename=$this->attributes['SAVETO'].'/'.time().rand(1000,9999);
            }
            if($_GET){
                foreach($_GET as $k=>$v){
                    if($k!='id'){
                        $this->$k=mysql_escape_string($v);
                        $this->dbfields[$k]=$this->$k;
                    }
                }
            }
            if($_POST){
                foreach($_POST as $k=>$v){
                    //$k=str_replace('iddif_','',$k);
                    $this->$k=stripslashes($v);
                    $this->dbfields[$k]=$this->$k;
                }
                //See if we have a pagetitle mapped
                if($this->attributes['PAGETITLE']){                    
                    $pagetitlefield=iddimysql::tidyname($this->attributes['PAGETITLE']);
                    $this->pagetitle=$this->$pagetitlefield;
                    $this->dbfields['pagetitle']=$this->pagetitle;
                }
                switch(strtolower($action)){
                    case 'save':
                        $this->id=$id;
                        $this->rootlanguageitemid=$this->id;
                        $this->save();
                        if($_SESSION['PREVIOUS_PAGE']){
                            header('Location: '.$_SESSION['PREVIOUS_PAGE']);
                            unset($_SESSION['PREVIOUS_PAGE']);
                            die();
                        }
                        break;
                        default:
                            $this->perform_action();
                        break;
                }
            }
            if($_GET['return']==1){
                $_SESSION['PREVIOUS_PAGE']=$_SESSION['LAST_PAGE'];
            }
            $_SESSION['LAST_PAGE']=$_SERVER['REQUEST_URI'];
        }

    function perform_action(){
        $action='action_'.iddiMySql::tidyname($_POST['action']);
        if(method_exists($this,$action)) $this->$action();
    }


        function parse(){
            $this->parse2();
            parent::parse();
        }

        function set_mode($mode){ $this->mode=$mode; }
        function get_mode() { return $this->mode; }

        /**
        * @desc Output back a valid xhtml form element
        */
        function output(){
            $output='<form ';
            $output.=' action="'.(($this->attributes['ACTION'])?$this->attributes['ACTION']:'').'"';
            $output.=' method="'.(($this->attributes['METHOD'])?$this->attributes['METHOD']:'post').'"';
            $output.=($this->attributes['ENCTYPE'])?' enctype="'.$this->attributes['ENCTYPE'].'"':'enctype="multipart/form-data"';
            $output.='>';
            if ($this->children) foreach($this->children as $child) $output.=$child->output($clean,($clean)?$level+1:0);
            $output.='</form>';

            return $output;
        }

        function getName(){return $this->getAttribute('ENTITY');}

        function compile(){
          $previous=$this->owner->getCurrentEntity();
          $this->owner->setCurrentEntity($this);
          echo '<li>Switching Entity To :'.$entity_name=$this->attributes['ENTITY'].'<ul>';
          parent::compile();
          iddiMySql::buildentitytable($this);
          echo '</ul>';
          $this->owner->setCurrentEntity($previous);
        }


    }


