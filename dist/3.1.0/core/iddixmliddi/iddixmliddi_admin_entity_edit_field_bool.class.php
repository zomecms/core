<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiXmlIddi_Admin_Entity_Edit_Field_Bool extends iddiXmlIddi_Admin_Entity_Edit_Field{
    function get_field($data){        
        $field_type = $this->field->type;
        $field_name = $this->field->fieldname;        
        if ($this->field->is_key || (isset($this->field->is_editable) && $this->field->is_editable===false)){
            $field = new iddiHtml_Div();
            $field->setValue($data->$field_name);
        }else{           
            $field = new iddiHtml_Div();
            $yes = iddiHtml_Input::build_radio($field_name,$field_name,1);
            $yes->attributes['DATA-ENTITY']=$data->entityname;
            $yes->attributes['DATA-ENTITYID']=$data->id;
            if($data->$field_name==1) $yes->attributes['CHECKED']='checked';
            $no = iddiHtml_Input::build_radio($field_name,$field_name,0);
            $no->attributes['DATA-ENTITY']=$data->entityname;
            $no->attributes['DATA-ENTITYID']=$data->id;
            if($data->$field_name!=1) $no->attributes['CHECKED']='checked';
            $field->appendChild($no);
            $field->appendChild($yes);
        }
        $new_col=$this->field_helper($this->field->caption, $field);
        $new_col->label->addclass('iddi-text-field');
        return $new_col;
    }    
}