<?php
    /**
    * iddiXmlIddi_Remove_Node Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Remove_Node extends iddiXmlIddiNode{
        function preparse(){
            $xpath=$this->getAttribute('select');
            $targets=$this->xpath($xpath,true);
            foreach($targets as $tgtnode) {
                $tgtnode->remove();
            }
            //$this->parent->removeChild($this);
        }
    }

    /**
    * @desc Access control
    */
   