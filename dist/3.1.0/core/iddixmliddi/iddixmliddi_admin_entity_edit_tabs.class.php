<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiXmlIddi_Admin_Entity_Edit_Tabs extends iddiXmlIddiNode{

    function parse(){
        foreach($this->children as $child){
            $child->is_first=true;
            break;
        }
        parent::parse();
    }
    
    function output(){
        $form=$this->getParentOfType('iddiXmlIddi_Admin_Entity_Edit');
        $out='<div class="tabs y-form-tabs"><ul>';
        $class=' class="active"';
        foreach($form->groups as $group){            
            $out.='<li'.$class.'>'.$group->caption.'</li>';
            $class='';
        }
        $out.='</ul>';
        $out.=parent::output();
        $out.='</div>';
        return $out;
    }
}