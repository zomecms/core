<?php
    /**
    * iddiXmlIddi_Php2 Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Php2 extends iddiXmlIddiNode{
        function parse(){
        }
        function output(){
            if(iddiRequest::getMode()!='edit'){
                $this->processAVT();
                $output=eval($this->getValue());
                return 'PHP'.$output;
            }else{
                return '<div class="editor-note"><p class="text">PHP Actions disabled in edit mode</p></div>';
            }

        }
    }
   