<?php
    /**
    * iddiXmlIddi_No_Output Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_No_Output extends iddiXmlIddiNode{
      function output(){
        return '';
      }
    }

    //---- Imaging ----//
   