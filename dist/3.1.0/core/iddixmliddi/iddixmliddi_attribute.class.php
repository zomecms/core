<?php
    /**
    * iddiXmlIddi_Attribute Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Attribute extends iddiXmlIddiNode{
        var $attrname,$select,$attrvalue;
        function parse(){
          parent::parse();
          $this->v=parent::output();
          $this->processAVT();
          $this->attrname=$this->getAttribute('NAME');
          if($this->v==''){
            $this->select=$this->getAttribute('SELECT');
            $this->attrvalue=$this->getFieldValue(iddiMySql::tidyname($this->select));
          }else{
            $this->attrvalue=$this->v;
          }
          $this->parent->setAttribute($this->attrname,$this->attrvalue);
        }

        function output(){
          return '';
        }
    }

    /**
    * @desc Base entity node - this actually provides a map to the requests primary datasource which will be an iddiPage object
    * @package Iddi_Datasources
    */
   