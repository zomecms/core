<?php
    /**
    * iddiXmlIddi_Form Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Form extends iddiEntity{
        function parse(){
            $this->_e=$this;
            $n=$this->getAttribute('ENTITY');
            //Split the name up at the hashes, which are fallbacks to the acutal datasource we want.
            //#new and #current are special
            $s=explode('#',$n);
            $this->entityname=$s[0];
            foreach($s as $nno=>$subn){
                if($nno>0){
                    switch($subn){
                        case 'new' : $this->id=--self::$_next_entity_id;
                            $got=true;
                            break;
                        case 'current':
                                if($_SESSION['current_datasource'][$this->entityname]){
                                    $this->id=$_SESSION['current_datasource'][$this->entityname];
                                }else{
                                    $this->id=--self::$nextid;
                                    $_SESSION['current_datasource'][$this->entityname]=$this->id;
                                }
                            $got=true;
                            break;
                        default:
                            if($_REQUEST[$subn]){
                                if($_REQUEST[$subn]=='new'){
                                    $this->id=--self::$_next_entity_id;
                                    $got=true;
                                }else{
                                    $this->id=$_REQUEST[$subn];
                                    //Load the data node
                                    try{
                                        $this->LoadById($this->entityname,$this->id);
                                        $got=true;
                                    }catch(Exception $e){

                                    }
                                }
                                break;
                            }
                    }
                }
                if ($got) break;
            }
            if(!$got){
                //Remove all the children except for no-entity
                foreach($this->children as $cnode){
                    if($cnode->nodename!='no-entity'){
                        //$cnode->remove();
                    }
                }
            }else{
                //Add the entity to the request
                iddiRequest::getMainRequest()->addDataSource($this);
                //Remove any no-entity value
                foreach($this->children as $cnode){
                    if($cnode->nodename=='no-entity'){
                        $cnode->remove();
                    }
                }
            }
            if(!$_POST){
                //no post so remove any failure or success notices
                foreach($this->children as $cnode){
                    if($cnode->nodename=='form-success' || $cnode->nodename=='form-failure'){
                        $cnode->remove();
                    }
                }

            }
            unset($this->attributes['ENTITY']);
            parent::parse();
        }
        /**
        * @desc Output back a valid xhtml form element
        */
        function output(){
            $output='<form ';
            $output.=' action="'.(($this->attributes['ACTION'])?$this->attributes['ACTION']:'').'"';
            $output.=' method="'.(($this->attributes['METHOD'])?$this->attributes['METHOD']:'post').'"';
            $output.=($this->attributes['ENCTYPE'])?' enctype="'.$this->attributes['ENCTYPE'].'"':'enctype="multipart/form-data"';
            $output.='><input type="hidden" name="context" value="save"/>';
            if ($this->children) foreach($this->children as $child) $output.=$child->output($clean,($clean)?$level+1:0);
            $output.='</form>';

            return $output;
        }
    }
    /**
    * @desc will output an xhtml input,select or textarea tag with the correct name for processing by iddi and attributes for it's type
    */
   