<?php
/**
 * iddiXmlIddi_Access_Control Class file
 * FOLLOWS the choice/switch pattern. This node is effectively the { }
 * This forms a container for a series of access control nodes. Each of the access control
 * nodes contained within this access control section will be switched on or off depending
 * on if the current user has the relevant privillege.
 * The reason for grouping them together is to allow for a final deny block if the current
 * user does not have access to any of the allow blocks.
 * @example
 * This example shows how to create a simple login area
 * <iddi:access-control>
 *  <iddi:access-control-allow allow="member">
 *   <p>You are logged in
 *    <ul>
 *     <li><a href="/my_account">My Account</a></li>
 *     <li><a href="/api/admin/logout">Logout</a></li>
 *    </ul>
 *   </p>
 *  </iddi:access-control-allow>
 *  <iddi:access-control-deny>
 *   You are not logged in
 *   <form method="post">
 *    <!-- LOGIN FORM HERE -->
 *   </form>
 *  </iddi:access-control-deny>
 * </iddi:access-control>
 *
 * The optional parameter of @editmode allows you to use this block in edit mode to
 * create areas that only certain staff can edit
 *
 * @author J.Patchett - Tastic Multimedia
 * @package IDDI Core
 **/
class iddiXmlIddi_Access_Control extends iddiXmlIddiNode {
    var $editcontrol=false;

    function pre_compile(){
        //check that we have at least one allow 
    }

    function parse() {
        $grant=false; //We use this to see if we need to remove the deny section
                      //will get set to true if we are allowed into any of the allow sections

        //Get the current user
        $current_user=$_SESSION['user'];
        if($current_user) $priv=explode(' ',$current_user->getValue('accesslevels'));
        
        //Locate the allow sections - areas that the user must have the relevant privillege for
        //in order to see
        $sections=$this->xpath('.//access-control-allow',true);
        if($this->attributes['EDITCONTROL']=='true' || iddiRequest::getMode()!='edit') {
            //Go through each of the sections in turn
            foreach($sections as $section) {
                $section->access_container=$this;
                $sectiongrant=false;
                foreach($section->getAttributes() as $atname=>$atvalue) {
                    if($atname=='GRANT') {
                        foreach($priv as $pv) {
                            if($pv==$atvalue) $sectiongrant=true;
                        }
                    }
                    //If we have not been allowed access to the section remove it
                    if($sectiongrant==false) $section->remove();
                    if($sectiongrant) $grant=true;
                }
            }
            //Finally if we've been allowed into any remove deny section
            if($grant) {
                $rem=$this->xpath('.//access-control-deny',true);
                foreach($rem as $n) $n->remove();
            }
        }
        parent::parse();
    }

    function get_editor_json($json){
        $this->edit_json=new stdClass();
        $this->edit_json->editor_class='iddi_access_control';
        parent::get_editor_json($json);
        $json->editors[]=$this->edit_json;
    }
    function _json_add_ac_state($state){
        $this->edit_json->states[]=$state;
        //Add any privileges that the state has provided
        foreach($state->privileges as $priv){
            $this->edit_json->privileges[$priv]=$priv;
        }
    }
    
}
