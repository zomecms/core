<?php
    /**
    * iddiXmlIddi_Sort Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Sort extends iddixmliddinode {
        var $sortfield;
        function preparse(){
            $this->sortfield=$this->getAttribute('SELECT');

            $qp=$this->getParentOfType('iddiXmlIddi_Query_Parser');
            $qp->sorter=$this;

            parent::preparse();
        }

        function buildSql_OrderBy(){
            return $this->sortfield;
        }
    }
    /**
    * @desc Generate a list of items in breadcrumb starting with home page and ending with current page
    */
   