<?php
    /**
    * iddiImages Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiImages extends iddiIterator{
        static function uploadFiles(){
          try{
            //All we do is create a thumbnail 95x95 pixels and dump that to the library, and the original to the imagestore
            //note: useable files will go to the images directory, and are first created on demand
            if($_SESSION['library']){
                $lib=$_SESSION['library'];
                if($lib=='') $lib='general';
                $storepath=IDDI_IMAGES_PATH.'/originals/'.$lib.'/';
                $librarypath=IDDI_IMAGES_PATH.'/library/'.$lib.'/';
                @mkdir(IDDI_IMAGES_PATH.'/originals');
                @mkdir($storepath);
                @chmod($storepath,0777);
                @chmod($librarypath,0777);
                $count=0;
                if(iddi::$debug) iddiDebug::dumpvar('Uploading files, count:'.sizeof($_FILES), $_FILES);
                //Check it's writeable
                foreach($_FILES as $postname=>$file){
                    print_r($file);
                  $count++;
                    if($file['name']){
                        if(iddi::$debug) iddiDebug::dumpvar('Uploading file '.$postname,$file);
                        //print_rp($file);
                        $filename=strtolower(preg_replace('/[^A-Za-z\.0-9_-]*/','',$file['name']));
                        if(iddiConfig::$overwriteUploads){
                            //Don't overwrite existing files - add a number after the filename
                            $fnum=1;
                            while(file_exists($storepath.$filename."($fnum)")){
                                $fnum++;
                            }
                            $filename=str_replace('.',"($fnum).",$filename);
                        }
                        $file['new_filename']=$filename;
                        if(move_uploaded_file($file['tmp_name'],$storepath.$filename)){
                            //make a thumbnail for the library if possible
                            if(substr($file['type'],0,6)=='image/'){
                              if(iddi::$debug) iddiDebug::message('Sending '.$storepath.$filename.' image for thumbnail');
                              $image=new iddiImage($storepath.$filename,155,95,'force');
                              $image->save($librarypath.$filename,60);
                              unset($image);
                            }else{
                              if(iddi::$debug) iddiDebug::message('Cannot send '.$file['type'].' for thumbnailing');
                            }
                        }else{
                            //Raise an error in here
                        }
                        $_POST[$postname]=$storepath.$filename;
                    }
                }
                if($count==1){
                  die('<script type="text/javascript">parent.iddiImageEditor.setImageSrc("'.$storepath.$filename.'");alert("Image Uploaded");</script>');
                }else{
                  die();
                }
            }
          }catch(Exception $e){
            iddiDebug::dumpexception('Image upload failed',$e);
            die('<script type="text/javascript">alert("Image upload failed "'.$e->getMessage().');</script>');
          }

        }

    }
