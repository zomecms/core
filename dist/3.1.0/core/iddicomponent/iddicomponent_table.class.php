<?php

class iddiComponent_Table{
    var $title;
    var $limit=200;

    function draw($entity,$filter=null,$use_fields=null){
        $e=new iddiEntityDefinition($entity);
        foreach($e->fielddefs as $field){
            $fields[$field->type][]=$field;
        }


        if($filter!==null){
            $filter=' AND ('.$filter.')';
        }

        $pagetitle=new stdClass();
        $pagetitle->caption="Name";
        $pagetitle->fieldname='pagetitle';
        $fields['text'][]=$pagetitle;

        $sql='SELECT * FROM iddi_sysfilenames f RIGHT JOIN {PREFIX}'.$entity.' t ON f.id=t.id AND f.entityname="'.$entity.'" WHERE (f.deleted is null or f.deleted=0) '.$filter.' ORDER BY f.odr, t.id DESC LIMIT 0,'.$this->limit;

        $d=iddiMySQL::query($sql);

        if($this->title=='') $this->title=$e->name.' List';

        $buttons.='<div><i class="fa fa-edit iddi-show-item" data-entity-id="-1" data-entity-name="'.$entity.'"><span>Add</span></i></div>';

        $out='<h1>'.$this->title.$buttons.'</h1>';

        /*
            if($fields['lookup'][0]){
                $main_item.='<fieldset>';
                foreach($fields['lookup'] as $f){

                    /**
                     * Get values
                     *
                    $lookups=array();
                    $lookups=iddiCache::get('LOOKUPSB_'.$f->fieldname);
                    if(!$lookups){
                        $lookups=array();
                        $sql='SELECT id,pagetitle FROM iddi_sysfilenames WHERE entityname="'.iddiMySql::tidyname($f->lookup).'" LIMIT 0,20';
                        $r=iddiMySql::query($sql);
                        foreach($r as $row) $lookups[$row->id]=$row->pagetitle;
                        iddiCache::save('LOOKUPSB_'.$f->fieldname, $lookups);
                    }
                    if(sizeof($lookups)<20){
                        $out.='<select name="filter['.$f->fieldname.'"]><option value="">'.$f->caption.'</option>';

                        foreach($lookups as $id=>$lookupitem){
                            $out.='<option value="'.$id.'"';
                            if($id==$item->dbfields[$f->fieldname]) $out.=' selected="selected"';
                            $out.='>'.$lookupitem.'</option>';
                        }

                        $out.='</select>';
                    }
                }
                $out.='</fieldset>';
            }
*/

        $out.='<table data-entity-name="'.$entity.'">';
        $out.='<tr>';
        $out.='<th></th>';
        $out.='<th>URL</th>';
        if(isset($fields['image'])){
            if($use_fields===null || in_array($f->fieldname, $use_fields)) $out.='<th>'.$fields['image'][0]->caption.'</th>';
        }
        foreach($fields['text'] as $f){
            if($use_fields===null || in_array($f->fieldname, $use_fields)) $out.='<th>'.$f->caption.'</th>';
        }
        $out.='</tr>';

        foreach($d as $row){
            $out.='<tr class="iddi-show-item" data-entity-id="'.$row->dbfields['id'].'" data-entity-name="'.$entity.'">';

            //Show ID and pagetitle
            $out.='<td>'.$row->dbfields['id'].'</td>';
            $out.='<td>'.$row->dbfields['virtualfilename'].'</td>';

            if(isset($fields['image'])){
                $out.='<td>';
                foreach($fields['image'] as $imagefield){
                    if($use_fields===null || in_array($f->fieldname, $use_fields)){
                        $imagedata=$row->dbfields[$imagefield->fieldname];
                        if(strlen($imagedata)<150){
                            if($imagedata!=''){
                                $out.='<img src="/api/image?width=40&height=40&scalemode=force&amp;outmode=image&amp;filename='.urlencode($imagedata).'"/>';

                            }
                        }
                    }
                }
                $out.='</td>';
            }


            //Show all text types first
            foreach($fields['text'] as $f){
                if($use_fields===null || in_array($f->fieldname, $use_fields)) $out.='<td>'.$row->dbfields[$f->fieldname].'</td>';
            }

            //Then numeric
            foreach($fields['number'] as $f){
                if($use_fields===null || in_array($f->fieldname, $use_fields)) $out.='<td>'.$row->dbfields[$f->fieldname].'</td>';
            }

            //Then bool

            //Then lookup

            $out.='</tr>';
        }
        $out.='</table>';
        return $out;
    }

}