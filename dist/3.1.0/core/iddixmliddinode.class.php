<?php

/**
 * iddiXmlIddiNode Class file
 * @author J.Patchett - Tastic Multimedia
 * @package IDDI Core
 * */
class iddiXmlIddiNode extends iddiXmlNode {

    /**
     * @desc Strips out iddi nodes, and just adds any children - override this to provide iddi functionality on a node
     */
    function output() {
        return $this->outputchildrenonly();
    }

    function outputchildrenonly() {
        if (is_array($this->children))
            foreach ($this->children as $child)
                $output.=$child->output();
        return $output;
    }

    function addclass($class) {
        return $this->parent->addClass($class);
    }

    function xpath($xpath, $includeiddinamespace = false, $resultset = null) {
        if ($includeiddinamespace) {
            return parent::xpath($xpath, true, $resultset);
        } else {
            if ($this->children) {
                foreach ($this->children as $child) {
                    $x = $child->xpath($xpath, false, $resultset);
                    if ($x != null)
                        return $x;
                }
            }
        }
    }

    function passthroughattributes($list = null) {
        if (is_array($list)) {
            foreach ($list as $attr) {
                if ($this->getAttribute($attr)) {
                    $output.=strtolower($attr) . '="' . strip_tags($this->getAttribute($attr)) . '" ';
                }
            }
        } else {
            foreach ($this->attributes as $attribute => $value) {
                $output.=strtolower($attribute) . '="' . strip_tags($value) . '" ';
            }
        }
        return trim($output);
    }

}
