<?php
    /**
    * iddiXmlObjectNode Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlObjectNode extends iddiXmlNode{
         function xpathinternal($strxpath,$includeiddinamespace=false,$resultset=null){
            if($resultset==null) $resultset=new iddiXpathResultSet();

            $xpath=new iddiXpath($strxpath);

          //Get the nodes to work on from the selected axis
            $axisdata=$this->getAxisData($xpath);


            if ($axisdata){
                foreach($axisdata as $axisnode){
                    if ($xpath->nodetest==$axisnode->nodename || $xpath->nodetest=='*' || $xpath->nodetest=='node()'){
                        //Check the predicates
                        if ($xpath->nodepredicates=='' || $axisnode->processPredicates($xpath->nodepredicates)){
                            if ($xpath->nextbit==''){
                                //End of the line, so add to result set
                                $resultset->addResult($axisnode);
                            } else {
                                //More processing, so pass the xslt on
                                if (method_exists($axisnode,'xpath')) $axisnode->xpath($xpath->nextbit,$includeiddinamespace,$resultset);
                            }
                        }
                    }
                }
            }
            return $resultset;
        }
        function getChildren(){return null;}
        function getDescendants(&$r=null){
            foreach($this->getDefinedVars() as $varname=>$varvalue){
                if(is_array($varvalue)){
                    foreach($varvalue as $child) $r[]=$child;
                }else{
                    $r[]=$child;
                }
                if(method_exists($varvalue,'getDescendants')) $varvalue->getDescendants($r);
            }
            return $r;
        }
        function getDescandantsOrSelf(){
            $r[]=$this;
            $this->getDescendants($r);
            return $r;
        }
        function getAncestors(&$r=null){
            if ($this->parentid>0){
                $p=$this->parent;
                $r[]=$p;
                $p->getAncestors($r);
            }else{
                $r[]=$this;
            }
            return $r;
        }
        function getAncestorsOrSelf(){
            $r[]=$this;
            $this->getAncestors($r);
            return $r;
        }
    }
   