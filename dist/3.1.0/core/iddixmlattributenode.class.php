<?php
    /**
    * iddiXmlAttributeNode Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlAttributeNode extends iddiXmlNode{
        protected $_v;
        function setvalue($value){
            $this->_v=$value; return $this;
        }
        function getvalue(){
            return $this->_v;
        }
    }


    //---- Data Sourcing ----

    /**
    * @desc Datasources provide useable data to templates. Every page request that results in successfully finding the relevant page in the database will load
    *       the relevant template, then provide the page data as a datasource. Any calls to data consumers such as iddi:value will use the current data source.
    *       Data locators such as iddi:use-entity or iddi:list-items will change the current active datasource.
    *       If you wish to write your own datasources you should normally extend this class, however you may find that extending iddiEntity provides more functionality
    *       as iddiEntity is generally the actual datasource that is used
    *       Datasources are treated as iddiXmlNodes
    * @package Iddi_Datasources
    */
   