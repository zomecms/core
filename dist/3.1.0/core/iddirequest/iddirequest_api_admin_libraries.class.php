<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiRequest_api_admin_libraries extends iddiApi_Result{
    function output(){        
        $this->libraries=$this->get_directories(IDDI_IMAGES_PATH.DIRECTORY_SEPARATOR.'originals');
        parent::output();
    }
    
    function get_directories($path,$maxdepth=5,$depth=0){
        $dirs=array();
        $d = opendir($path);
        while ($f = readdir($d)) {
            if($f!=='.' && $f!=='..' && is_dir($path.DIRECTORY_SEPARATOR.$f)){
                $dir=new stdClass();
                $dir->name=$f;
                $dir->caption=  ucwords(str_replace('_',' ',$f));
                $dirs[]=$dir;                
                $subdirs=$this->get_directories($path.DIRECTORY_SEPARATOR.$f,$maxdepth,$depth+1);
                if(sizeof($subdirs)>0){
                    $dir->has_children=true;
                    $dir->subdirs=$subdirs;
                }else{
                    $dir->has_children=false;
                }
            }
        }
        return $dirs;
    }
}