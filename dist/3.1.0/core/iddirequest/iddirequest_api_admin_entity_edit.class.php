<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiRequest_api_admin_entity_edit extends Api_Result {

    function output() {             
        $entity_name=$_GET['e'];
        $id=intval($_GET['i']);
        $_REQUEST['ajax']=1;

        $datasource=($entity_name!='' && $id > 0) ? new iddiEntity($entity_name,$id) : null;
        return iddiTemplate::render(array('entity_edit_'.strtolower($entity_name),'entity_edit'), $datasource);
    }

}
