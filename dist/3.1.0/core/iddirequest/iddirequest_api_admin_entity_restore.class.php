<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiRequest_api_admin_entity_restore extends Api_Result{
    function output(){
        $eclass='iddiEntity_'.$_POST['entityname'];
        if(class_exists($eclass)){
            $entity=new $eclass();
        }else{
            $entity=new iddiEntity();
        }
        if($_POST['entityid']>0){
            $entity->get_by_id($_POST['entityid']);            
            unset($_POST['entityname']);
            unset($_POST['entityid']);
            foreach($_POST as $k=>$v){
                if($entity->pagetitle=='' && $_POST['pagetitle']==''){
                    $entity->pagetitle=$v;
                }
                $entity->$k=$v;
            }

            $entity->deleted=0;
            
            $entity->save();
        }

        header('Content-type: text/json');
        
        $out=new stdClass();
        $out->entity=$entity;
        $out->message='Item Restored';
        
        die(json_encode($out));

    }
}