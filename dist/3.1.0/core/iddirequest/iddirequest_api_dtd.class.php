<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

define('YENDOI_SCANNING_DTD');

class iddiRequest_api_dtd extends Api_Result{
    function output(){        
        $this->items[]=new dtd_item('value');
        
        foreach($this->items as $item){
            echo $item->output();
        }
    }
    
}

class dtd_item{
    var $element_name;
    var $allowable_children='ALL';
    var $attributes=array();
    
    function __construct($name){
        $this->element_name=$name;
        $filename=__DIR__.'/../iddixmliddi/iddixmliddi_'.$name.'.class.php';
        include $filename;
    }
    
    function add_attribute($name){
        $attribute=new dtd_attribute($name);        
        $this->attributes[$name]=$attribute;
        return $attribute;
    }
    
    function output(){
        $out='<!ELEMENT '.$this->element_name.' ANY>';
        $out.="\r\n";
        foreach($this->attributes as $attribute){
            $out.=$attribute->output($this);
        }
        $out.="\r\n";
        return $out;
    }
}

class dtd_attribute{
    var $_name;
    var $_type = self::TYPE_CDATA;
    var $_value = self::VALUE_REQUIRED;
    
    const VALUE_REQUIRED = '#REQUIRED';
    const VALUE_IMPLIED = '#IMPLIED';
    
    const TYPE_CDATA = 'CDATA';
    const TYPE_ID = 'ID';
    const TYPE_IDREF = 'IDREF';
    const TYPE_IDREFS = 'IDREFS';
    const TYPE_NMTOKEN = 'NMTOKEN';
    const TYPE_NMTOKENS = 'NMTOKENS';
    const TYPE_ENTITY = 'ENTITY';
    const TYPE_ENTITIES = 'ENTITIES';
    const TYPE_NOTATION = 'NOTATION';
    
    function __construct($name){
        $this->_name=$name;
    }
    
    function type($type=null){
        if($type!==null){
            $this->_type=$type;
            return $this;
        }
        return $this->_type;
    }
    
    function value($value=null){
        if($value!==null){
            $this->_value=$value;
            return $this;   
        }
        return $this->_value;
    }
    
    function name($name=null){
        if($value!==null){
            $this->_name=$name;
            return $this;   
        }
        return $this->_name;
    }
    
    
    function output(dtd_item $element){
        $out='<!ATTLIST ';
        $out.=$element->element_name.' ';
        $out.=$this->name().' ';
        $out.=$this->type().' ';
        $out.=$this->value().'>';
        $out.="\r\n";
        return $out;
    }
}