<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiRequest_api_admin_entity_delete extends Api_Result {

    function output() {
        $entity_name = filter_input(INPUT_POST, 'entity_name', FILTER_SANITIZE_STRING);
        $entity_id = filter_input(INPUT_POST, 'entity_id', FILTER_SANITIZE_NUMBER_INT);

        if ($entity_id <= 0)
            throw new iddiException('Parameter entity_id must be a positive integer', 'zome.api.entity_delete.invalid_entity_id');

        try {
            $eclass = 'iddiEntity_' . $entity_name;
            if (class_exists($eclass)) {
                $entity = new $eclass();
                $entity->get_by_id($entity_id);
            } else {
                $entity = new iddiEntity();
                $entity->get_by_id($entity_id, $entity_name);
            }
            $this->classname = get_class($entity);

            $entity->deleted = 1;
            $entity->save();
            $entity->delete();

            $this->success = true;
            $this->message = $entity_name . ' #' . $entity_id . ' deleted';
        } catch (Exception $e) {
            $this->success = false;
            $this->message = $entity_name . ' #' . $entity_id . ' failed to delete (' . $e->getMessage() . ')';
        }
        return parent::output();
    }

}
