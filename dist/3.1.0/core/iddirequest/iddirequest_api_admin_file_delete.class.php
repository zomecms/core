<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiRequest_api_admin_file_delete extends iddiApi_Result {

    function output() {
        $library = filter_input(INPUT_GET, 'library', FILTER_SANITIZE_STRING);
        $file = filter_input(INPUT_GET, 'file', FILTER_SANITIZE_STRING);

        if (strstr($library, '/')) {
            throw new iddiException('Invalid Library : ' . $library, 'yendoi.api.file_delete.invalid_library');
        }
        if (strstr($file, '/')) {
            throw new iddiException('Invalid File', 'yendoi.api.file_delete.invalid_file');
        }
        $library_path = IDDI_IMAGES_PATH . DIRECTORY_SEPARATOR . 'originals' . DIRECTORY_SEPARATOR . $library;
        if (!file_exists($library_path)) {
            throw new iddiException('Invalid Library : ' . $library, 'yendoi.api.file_delete.invalid_library');
        }
        $filename = $library_path . DIRECTORY_SEPARATOR . $file;
        if (!file_exists($filename)) {
            throw new iddiException('Invalid File', 'yendoi.api.file_delete.invalid_library');
        }
        
        unlink($filename);
        
        $this->success=true;
        
        parent::output();
    }

}
