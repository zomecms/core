<?php
    /**
    * iddiBreakpoint Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiBreakpoint extends iddiException{
        function __construct($message='breakpoint'){
          if(is_object($message)){
            if(method_exists($message,'dump')) $message=$message->dump(1);
          }
          parent::__construct($message,'breakpoint');
        }
    }
   