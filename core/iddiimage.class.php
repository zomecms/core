<?php
    /**
    * iddiImage Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiImage extends iddiEvents{
        var $processor;

        function __construct($inputfilename,$outputheight=0,$outputwidth=0,$scalemode='crop',$preventupscale=false){
            /**
             * Determine the processor based on config
             */
            //if(iddiConfig::GetValue('kraken', 'api-key')){
            //    $this->processor=new iddiImageKraken($inputfilename,$outputheight=0,$outputwidth=0,$scalemode='crop',$preventupscale=false);
            //}else{
                $this->processor=new iddiImageGd($inputfilename,$outputheight=0,$outputwidth=0,$scalemode='crop',$preventupscale=false);
            //}

            $this->inputfilename=$inputfilename;

            if ($inputfilename!='' && $outputheight>0 && $outputwidth>0){
                $this->resize($outputheight,$outputwidth,$scalemode,$preventupscale);
            }
        }

        function resize($outputheight,$outputwidth,$scalemode='crop',$preventupscale=false){
            $this->processor->resize($outputheight,$outputwidth,$scalemode,$preventupscale);
        }

        function __destruct(){
            $this->processor->destroy();
        }

        function save($filename,$quality=90){
            $this->processor->save($filename,$quality);
        }
    }

    //---- Core page handling system -----
