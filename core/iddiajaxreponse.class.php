<?php
    /**
    * iddiAjaxReponse Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiAjaxReponse{
        var $errors,$objects,$stats,$commands;
        var $content;
        function addError($message,$code='iddi.ajax.unknown',$type='warning'){
          $count=1;
          if ($this->errors[$code]){
            $existing_error=$this->errors[$code];
            $count=$existing_error['counter']+1;
          }
          $this->errors[$code]=array('message'=>$message,'code'=>$code,'type'=>$type,'counter'=>$count);
        }
        function addCommand($command,$params){
            $this->commands[]=array('command'=>$command,'params'=>$params);
        }
        function addObject($object){
            $this->objects[]=$object;
        }
        function addStat($stat,$value){
            $this->stats[$stat]=$value;
        }
    }
   