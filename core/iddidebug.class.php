<?php
    /**
    * iddiDebug Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiDebug extends iddiEvents{
        static function dumpvar($message,$var){
            if (iddi::$debug) {
                if (method_exists($var,'dump')){
                    $v=$var->dump(4);
                }else{
                    $v='<pre>'.print_r($var,true).'</pre>';
                }
                self::message($message,$v);
            }
        }
        static function dumpexception($message,$var){
            if (iddi::$debug) {
                if (method_exists($var,'dump')){
                    $v=$var->dump(4);
                }else{
                    $v=$var;
                }
                self::message($message,$v);
            }
        }
        
        static function warning($message,$code,$detail){
            
        }
        
        static function message($message,$detail=''){
            //Disabling totally for now as this needs a total rewrite.
            return;
            
            global $START_TIME;
            if (IDDI_ENABLE_DEBUG_MESSAGES_TO_SCREEN) echo "\n$message<br/>";
            if (iddi::$debug) {
                $timer=intval((microtime(true)-$START_TIME)*100);
                if(iddiRequest::$current->ismainrequest){
                    $f='debugger/'.session_id().'/Request-'.iddiRequest::$current->requestid;
                }else{
                    $f='debugger/'.session_id().'/Request-'.iddiRequest::getMainRequest()->requestid.'/Child-Request-'.iddiRequest::$current->requestid;
                }
                if (!file_exists($f)){
                    @mkdir('debugger');
                    //@chmod('debugger',0777);
                    //@mkdir('debugger/'.session_id());
                    //@chmod('debugger/'.session_id(),0777);
                    if(!iddiRequest::$current->ismainrequest){
                        $f2='debugger/'.session_id().'/Request-'.iddiRequest::getMainRequest()->requestid;
                        @mkdir($f2);
                        @chmod($f2,0777);
                    }
                    //Make a directory for the request
                    @mkdir($f);
                    @chmod($f,0777);
                }
                //open the log and add the message
                if ($detail!=''){
                    $detailid=++$_SESSION['detailid'];
                    $fp=fopen($f.'/log.txt','a');
                    fwrite($fp,date('Y-m-d H:i:s',time()).':'.$timer."ms:\r<a href=\"{$f}/{$detailid}.htm\" target=\"detail\">{$message}</a>\n");
                    fclose($fp);
                    $fp=fopen($f.'/'.$detailid.'.htm','w');
                    fwrite($fp,$detail);
                    fclose($fp);
                }else{
                    $fp=fopen($f.'/log.txt','a');
                    fwrite($fp,date('Y-m-d H:i:s',time())."\t$message\n");
                    fclose($fp);
                }
                //Add to the general log
                $fp=fopen('debugger/'.session_id().'/all5.txt','a');
                if ($detail!=''){
                    $premessage=$timer.'ms | '.(round(memory_get_usage(true)/1024/1024,2)).'Mb';
                    $message="<a href=\"../{$f}/{$detailid}.htm\" target=\"detail\">{$message}</a>\n";
                }
                if(iddiRequest::$current->ismainrequest){
                    fwrite($fp,'<div><span class="info">'.date('Y-m-d H:i:s',time()).' R:'.iddiRequest::$current->requestid." {$premessage}</span<span>{$message}</span></div>\n");
                }else{
                    fwrite($fp,'<div><span class="info">'.date('Y-m-d H:i:s',time()).' R:'.iddiRequest::getMainRequest()->requestid.'/'.iddiRequest::$current->requestid."</span><span>{$message}</span></div>");
                }
                fclose($fp);

            }
        }
    }


    //---- XML Base Stuff ----
   