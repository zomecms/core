<?php
    /**
    * iddiPlugins Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiPlugins extends iddiEvents{
        function __construct(){
            //Load the plugin handlers only at this point
            //Each plugin handler needs to be as tiny as possible - where possible merge them into one file
            //The plugin handlers can determine which actual plugins are required for any given request
            $d=opendir(IDDI_PLUGINS_PATH);
            while($f=readdir($d)){
                if (substr($f,-9)=='plg.h.php'){
                    require_once(IDDI_PLUGINS_PATH.'/'.$f);
                }
            }
        }
    }
