<?php

/**
 * iddiException_Request
 * 
 * Signifies some kind of problem in the initial request or request processing
 * 
 * @author J.Patchett 
 * @package IDDI Core
 * */
class iddiException_Request extends iddiException {
    
}