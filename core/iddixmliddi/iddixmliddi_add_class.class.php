<?php
/**
 * iddiXmlIddi_Add_Class Class file
 * Adds a class to the node provided by @select
 * @param @select Xpath to the node(s) in the template to add a class to
 * @param text() The contents of this node is added to the selected node(s)
 * @example
 * This example adds a class of myclass to the body
 * <iddi:add-class select="body">myclass</iddi:add-class>
 *
 * This example adds a class of the current page id to the body
 * <iddi:add-class select="//body">id-<iddi:value-of select="current()/id"/></iddi:add-class>
 *
 * @author J.Patchett - Tastic Multimedia
 * @package IDDI Core
 **/
class iddiXmlIddi_Add_Class extends iddiXmlIddiNode {

    /**
     * On the parse we can add any classes
     */
    function parse(){
        $this->pre_compile();
        $targets=$this->xpath($this->attributes['SELECT'],true);
        if($targets) foreach($targets as $tgtnode) $tgtnode->addClass($this->value);
    }

    /**
     * We do not want to output this node or any of it's contents - it's a dead end
     */
    function output(){}
}

