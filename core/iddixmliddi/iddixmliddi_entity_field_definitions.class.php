<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiXmlIddi_Entity_Field_Definitions extends iddiXmlIddi_List_Items{
    function get_data(){        
        $this->processAVT();
        $entityname=iddiMySql::tidyname($this->attributes['ENTITY']);
        $fieldtype=iddiMySql::tidyname($this->attributes['TYPE']);
        $sql="SELECT distinct entityname,fieldname,type,caption,lookup from `{PREFIX}sysentityfields` WHERE `entityname`='{$entityname}'";
        if($fieldtype){
            $sql.=' AND `type` = \''.$fieldtype.'\'';
        }
        return iddiMySql::query($sql);
    }
}