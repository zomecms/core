<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiXmlIddi_Dashboard_Headline extends iddiXmlIddi_Dashboard_Headline_Base {
    var $stat='COUNT(id)';
    var $title='?';
    var $subtitle='Last 30 Days';
    
    function preparse(){
        $template=new iddiXmlIddi_Insert_Template();
        $template->setAttribute('source', 'headline');
        $template->setAttribute('select', '//div');
        $this->appendChild($template);
        parent::preparse();        
    }
    
    function parse(){
        if($this->attributes['ENTITY']) $this->table='iddi_'.$this->attributes['ENTITY'];
        if($this->attributes['FILTER']) $this->where=$this->attributes['FILTER'];
        if($this->attributes['STAT']) $this->stat=$this->attributes['STAT'];
        if($this->attributes['TITLE']) $this->title=$this->attributes['TITLE'];
        if($this->attributes['SUBTITLE']) $this->subtitle=$this->attributes['SUBTITLE'];
        if($this->attributes['STATPREFIX']) $this->headline_prefix=$this->attributes['STATPREFIX'];
        

        return parent::parse();
    }
}