<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiXmlIddi_Dashboard_Component extends iddiXmlIddiNode {
    function parse(){
        $this->component=$this->attributes['COMPONENT'];
        //This is used to track which components have actually been requested so 
        //we prevent the api from serving up components to malicious clients
        $_SESSION['script'][$this->component]=1;
        
        $dashboard=$this->getParentOfType(iddiXmlIddi_Dashboard);
        $dashboard->add_component($this->component);
    }
    
    function output(){
        return '<div class="y-dashboard-component-holder '.$this->attributes['CLASS'].'" data-holder-for="'.$this->component.'"></div>';
    }
}
