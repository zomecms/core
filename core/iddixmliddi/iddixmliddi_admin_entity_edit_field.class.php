<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiXmlIddi_Admin_Entity_Edit_Field extends iddiXmlIddiNode{
    var $field;
    var $form;
        
    function add_to_form(iddiXmlIddi_Admin_Entity_Edit $form){
        $this->set_form($form);
        
        $fieldname=$this->attributes['NAME'];
        $this->field=$form->entity_definition->fielddefs[$fieldname];
        
        if($this->attributes['EDITABLE'])
            $this->field->is_editable=($this->attributes['EDITABLE']==='true');
                
        if(is_object($this->field) && $this->field instanceOf iddiEntityField) $form->add_field($this);
        
    }
    
    function set_form($form){
        $this->form=$form;
        $this->set_data();
        $this->load_template();       
    }
    
    function set_data(){                
        $field_name = $this->field->fieldname;                
        $fielddata=new iddiDataSource();        
        $fielddata->dbfields['caption']=$this->field->caption;
        $fielddata->dbfields['fieldvalue']=$this->form->row->$field_name;
        $fielddata->dbfields['fieldname']=$field_name;
        $fielddata->dbfields['id']=$this->form->entityid;
        $fielddata->dbfields['entity']=$this->form->entity;
        $this->setDataSource($fielddata);
    }    
    
    function load_template(){
        $field_type = $this->field->type;
        $field_name = $this->field->fieldname;
        if($this->field->can_edit===false){
            $t=iddiTemplate::load_first_match(array('readonly_field_'.$this->form->entity.'_'.$field_name,'readonly_field_type_'.$field_type,'readonly_field'), $fielddata)->data->documentelement;
        }else{
            $t=iddiTemplate::load_first_match(array('edit_field_'.$this->form->entity.'_'.$field_name,'edit_field_type_'.$field_type,'edit_field'), $fielddata)->data->documentelement;    
        }
        foreach($t->children as $child) $this->appendChild($child);        
    }
      
}