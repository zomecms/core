<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiXmlIddi_Admin_Entity_Table extends iddiXmlIddiNode {

    /**
     * Array of field definition objects to use when building the table
     * By default this will come from the entity definition and will include all fields
     * from the entity.
     * You can override by providing a comma seperated list of field names in
     * the @fields attributes
     * For more control override with iddi:admin-entity-table-field or a subclass of
     * @var array
     */
    var $fields = array();
    var $dbfields = array();
    var $entity_definition;
    var $entity,
            $filter,
            $page = 1,
            $page_size = 5,
            $quicksearch,
            $totalRecords,
            $totalPages;

    function parse() {
        $this->processAVT();
        $this->entity = $this->attributes['ENTITY'];
        $this->filter = $this->attributes['FILTER'];
        $this->page = ($_GET['page']) ? $_GET['page'] : 1;
        $this->page_size = ($_GET['page_size']) ? $_GET['page_size'] : ($this->attributes['PAGESIZE']) ? $this->attributes['PAGESIZE'] : 10;
        $this->quicksearch = ($_GET['quicksearch']) ? urldecode($_GET['quicksearch']) : null;

        $this->entity_definition = new iddiEntityDefinition($this->entity);

        $allfields = $this->get_fields();
        $data = $this->get_data($this->entity, $this->filter, $this->page);

        $table = $this->prepare_table();
        foreach ($allfields as $k => $v) {
            $th = $v->get_heading_cell();
            if ($th !== null)
                $this->table_head->appendChild($th);
        }

        foreach ($data as $row) {
            $this->currentrow = $row;
            $tr = new iddiHtml_TR();
            foreach ($allfields as $k => $v) {
                $field = $v->get_value_cell($row->dbfields[$k]);
                if ($field !== null)
                    $tr->appendChild($field);
            }
            $this->table_body->appendChild($tr);
        }

        $this->appendChild($table);

        parent::parse();

        return $out;
    }

    function can_edit_cells() {
        return (!isset($this->attributes['EDITABLE-CELLS']) || $this->attributes['EDITABLE-CELLS'] = 'true');
    }

    function prepare_table() {
        $this->table = new iddiHtml_Table();
        $this->table_body = new iddiHtml_TBody();
        $this->table_header = new iddiHtml_THead();
        $this->table_head = new iddiHtml_TR();
        $this->table_header->appendChild($this->table_head);

        $this->table->appendChild($this->table_header);
        $this->table->appendChild($this->table_body);

        return $this->table;
    }

    function add_row($row) {
        $new_row = new iddiHtml_TR();

        if ($this->children)
            foreach ($this->children as $child)
                if (method_exists($child, 'add_row'))
                    $new_row = $child->add_row($new_row, $row);

        $this->table_head->appendChild($new_row);
        return $new_row;
    }

    function get_fields() {
        $fixed_front_fields = array(
            'id' => $this->id_field(),
            'edit_action' => $this->edit_action(),
            'pagetitle' => $this->pagetitle_field()
        );

        $fixed_end_fields = array(
            'created' => $this->created_field(),
            'lock_action' => $this->lock_action(),
            'delete_action' => $this->delete_action()
        );

        $this->entity_definition->fielddefs = array_merge(
                $fixed_front_fields, $this->entity_definition->fielddefs, $fixed_end_fields
        );

        if ($this->children)
            foreach ($this->children as $child)
                if ($child instanceOf iddiXmlIddi_Admin_Entity_Table_Field)
                    $child->add_to_table($this);

        if (sizeof($this->fields) == 0)
            $this->auto_set_fields();

        return $this->fields;
    }

    function auto_set_fields() {
        if ($this->attributes['FIELDS']) {
            $usefields = explode(',', $this->attributes['FIELDS']);

            $this->append_field('id');
            $this->append_field('edit_action');
            foreach ($usefields as $field)
                $this->append_field($field);
            $this->append_field('lock_action');
            $this->append_field('delete_action');
        } else {
            foreach ($this->entity_definition->fielddefs as $field)
                $this->append_field($field);
        }
    }

    function append_field($fieldname) {
        if (is_string($fieldname)) {
            if (!isset($this->entity_definition->fielddefs[$fieldname]))
                return;
            $f = $this->create_field($this->entity_definition->fielddefs[$fieldname]);
        }elseif ($fieldname instanceof iddiEntityField) {
            $f = $this->create_field($fieldname);
        } else {
            throw new iddiException('Invalid Field', 'iddi.admin.entity_table.append_field.invalid_input');
        }
        $f->table = $this;
        $this->add_field($f);
    }

    /**
     * Adds a new field to the list of fields to render
     * @param iddiXmlIddi_Admin_Entity_Table_Field $field
     */
    function create_field(iddiEntityField $field) {
        $newfield = new iddiXmlIddi_Admin_Entity_Table_Field();
        $newfield->field = $field;

        $this->appendChild($newfield);

        return $newfield;
    }

    /**
     * Adds a new field to the list of fields to render
     * @param iddiXmlIddi_Admin_Entity_Table_Field $field
     */
    function add_field(iddiXmlIddi_Admin_Entity_Table_Field $field) {
        $this->fields[$field->field->fieldname] = $field;
    }

    /**
     * Removes the given field frm the fields to render
     * Can also pass in a string
     * If there are no matches then nowt happens
     * @param iddiEntityField $field
     */
    function remove_field($field) {
        if (is_string($field)) {
            unset($this->fields[$field]);
        } elseif (is_object($field) && $field instanceOf iddiEntityField) {
            unset($this->fields[$field->fieldname]);
        }
    }

    function id_field() {
        $id = new iddiEntityField();
        $id->caption = "#";
        $id->fieldname = 'id';
        $id->is_key = true;
        return $id;
    }

    function created_field() {
        $id = new iddiEntityField();
        $id->fieldname = 'created';
        $id->type = 'datetime';
        $id->caption = 'Created';
        $id->can_edit = false;
        return $id;
    }

    function pagetitle_field() {
        $pagetitle = new iddiEntityField();
        $id->fieldname = pagetitle;
        $pagetitle->caption = "Name";
        $pagetitle->fieldname = 'pagetitle';
        return $pagetitle;
    }

    function edit_action() {
        $edit = new iddiEntityField();
        $edit->fieldname = 'edit_action';
        $edit->is_action = true;
        $edit->action = 'y-action-show-item';
        $edit->type = 'action';
        $edit->icon = 'fa-edit';
        return $edit;
    }

    function lock_action() {
        $lock = new iddiEntityField();
        $lock->fieldname = 'lock_action';
        $lock->is_action = true;
        $lock->action = 'y-action-lock-item';
        $lock->type = 'action';
        $lock->icon = 'fa-lock';
        return $lock;
    }

    function delete_action() {
        $delete = new iddiEntityField();
        $delete->fieldname = 'delete_action';
        $delete->is_action = true;
        $delete->action = 'y-action-delete';
        $delete->type = 'action';
        $delete->icon = 'fa-trash';
        return $delete;
    }

    function get_data() {
        if ($this->filter !== null) {
            $this->filter = ' AND (' . $this->filter . ')';
        }

        if ($this->quicksearch !== null) {
            $quicksearch_fields = array();
            $quicksearch = json_decode($this->quicksearch);
            if ($quicksearch->quicksearch) {
                $quicksearch_items = explode(' ', strtolower($quicksearch->quicksearch));
                $normalized_search = preg_replace('[^a-z0-9]', '', strtolower($quicksearch->quicksearch));
                $quicksearch_items[] = $normalized_search;
                foreach ($this->fields as $field) {
                    if ($field->field->type == 'text' && $field->field->fieldname !== 'pagetitle') {
                        foreach ($quicksearch_items as $search_text) {
                            $quicksearch_fields[] = '(LOWER(`t`.`' . $field->field->fieldname . '`) like \'' . $search_text . '%\' or LOWER(`t`.`' . $field->field->fieldname . '`) like \' ' . $search_text . '%\')';
                        }
                    }
                    if (($field->field->type == 'number' || $field->field->fieldname == 'id') && intval($quicksearch->quicksearch) > 0) {
                        $quicksearch_fields[] = '`t`.`' . $field->field->fieldname . '` = ' . intval($quicksearch->quicksearch);
                    }
                }
                foreach ($quicksearch_items as $search_text) {
                    $quicksearch_fields[] = '(LOWER(`f`.`pagetitle`) like \'' . $search_text . '%\' or LOWER(`f`.`pagetitle`) like \' ' . $search_text . '%\')';
                }
            }
            foreach($quicksearch as $k=>$v){
                if($k!='quicksearch'){
                    $quicksearch_fields_and[] = '`t`.`' . $k . '` = ' . intval($v);
                }
            }
            if(sizeof($quicksearch_fields) > 0) $this->filter .= ' AND ( ' . implode(' OR ', $quicksearch_fields) . ')';
            if(sizeof($quicksearch_fields_and) > 0) $this->filter .= ' AND ( ' . implode(' AND ', $quicksearch_fields_and) . ')';            
        }

        $this->sql = 'SELECT SQL_CALC_FOUND_ROWS  * FROM iddi_sysfilenames f RIGHT JOIN {PREFIX}' . $this->entity . ' t ON f.id=t.id AND f.entityname="' . $this->entity . '" WHERE (f.deleted is null or f.deleted=0) ' . $this->filter . ' ORDER BY f.odr, t.id DESC LIMIT ' . (($this->page - 1) * $this->page_size) . ',' . $this->page_size;

        if ($this->children)
            foreach ($this->children as $child)
                if (method_exists($child, 'get_data_sql'))
                    $this->sql = $child->get_data_sql($this->sql, $this->entity, $this->filter);

        $d = iddiMySQL::query($this->sql);

        $this->totalRecords = iddiMySql::query('SELECT FOUND_ROWS()')->getFirstRow()->rawDbData['found_rows()'];
        $this->totalPages = ceil($this->totalRecords / $this->page_size);

        return $d;
    }

}
