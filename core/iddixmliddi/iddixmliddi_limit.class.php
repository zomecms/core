<?php
    /**
    * iddiXmlIddi_Limit Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Limit extends iddixmliddinode {
        var $start=0,$length=1000,$end;
        function preparse(){
            $this->start=$this->getAttribute('START');
            $this->end=$this->getAttribute('END');
            $this->length=$this->getAttribute('LENGTH');

            if ($this->start=='')  throw new iddiException('You must specify a start attribute','iddi.iddiXml.LimitNode.nostart',$this);
            if ($this->end!='' && $this->length!='')  throw new iddiException('You can only specify an end OR a length attribute, not both on a limit node','iddi.iddiXml.LimitNode.endandlimit',$this);
            if ($this->end=='' && $this->length=='')  throw new iddiException('You must specify an end OR a length attribute','iddi.iddiXml.LimitNode.noendorlimit',$this);
            if ($this->end!='' && $this->length=='') $this->length=$this->end-$this->start;

            $qp=$this->getParentOfType('iddiXmlIddi_Query_Parser');
            $qp->limiter=$this;

            parent::preparse();
        }

        function buildSql_Limit(){
            return $this->start.','.$this->length;
        }
    }
