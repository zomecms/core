<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiXmlIddi_Dashboard_Flot_Sales_Pie extends iddiXmlIddi_Dashboard_Flot_Pie_Base {

    var $stat = 'COUNT(id)';
    var $title = 'Product Sales';
    var $subtitle = 'Last 30 Days';

    function get_data() {
        $sql = 'SELECT pagetitle as label ,SUM(price_at_time) as value
                FROM iddi_orderitem i
                INNER JOIN iddi_sysfilenames p ON p.id=i.item_id
                INNER JOIN iddi_order o ON o.id=i.order_id
                WHERE balance_to_pay<=0 AND total_price>0
                GROUP BY pagetitle 
                ORDER BY `value` DESC';

        return iddiMySql::query($sql);
    }

}
