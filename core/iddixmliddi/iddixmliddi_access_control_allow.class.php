<?php
/**
 * iddiXmlIddi_Access_Control_Allow Class file
 * These sections in a template will only display if the current logged in user
 * has the required privileges.
 * Use this node withing an access-control node to be able to create a fallback for
 * users that do not have access
 * Can be used standalone if no fallback is required
 *
 * See /core/iddixmliddi/iddixmliddi_access_control.class.php for an example
 *
 * @author J.Patchett - Tastic Multimedia
 * @package IDDI Core
 **/
class iddiXmlIddi_Access_Control_Allow extends iddiXmlIddiNode {
    var $editable=false;
    protected $_force_allow=false;  //Force this block to be allowed - this is used to gather the output for the editor

    function preparse() {
        $all=$this->xpath('.//value',true);
        foreach($all as $one) {
            if ($one->nodename=='value') $this->editable=true;
        }
    }
    function output() {
        if(iddiRequest::getMode()=='edit' || $this->_force_allow ) {
            if($this->editable && !$this->access_container->editcontrol=='true') {
                $output='<div class="editor-note"><p class="text">This section displays if the current visitor has the following privilege(s):';
                foreach($this->getAttributes() as $atname=>$atvalue) {
                    if($atname=='GRANT') $output.='<br/>'.$atvalue;
                }
                $output.='</p>';
            }
        }
        $output.=parent::output();
        if(iddiRequest::getMode()=='edit' && !$this->access_container->editcontrol=='true') {
            $output.='</div>';
        }
        return $output;
    }
    
    function get_editor_json($json){
        $this->_force_allow=true;

        $json_obj=new stdClass();
        $json_obj->name='access_control_allow';
        $json_obj->content=$this->output();
        //Build the privileges list that trigger this block
        foreach($this->getAttributes() as $atname=>$atvalue) {
            if($atname=='GRANT'){
                $json_obj->privileges[$atvalue]=$atvalue;
            }
        }

        //Add to the access control block if available
        //Otherwise just to the json object
        $access_control_block=$this->getParentOfType('iddiXmlIddi_access_control');
        if($access_control_block){
            $access_control_block->_json_add_ac_state($json_obj);
        }else{
            $json->editors[]=$json_obj;
        }
        
        $this->_force_allow=false;
    }
}

/*
 * This needs redoing a bit - Ideally in edit mode we'd like to see a list of the
 * privileges being checked for, then allow the operator to select one or more
 * privileges to see what a user with those privileges would see
 *
 * For this to work we'll need:
 *
 * 1: To gather a list of privileges being checked for
 * 2: The ability for the editor to view each of the options, and to know which goes with
 *    each privileges
 *
 * Questions:
 *
 * Do we download all versions to the editor - maybe with a class so the editor knows?
 *
 * Maybe we could generate some additional json that contains the alternatives - I like this possiblity
 */