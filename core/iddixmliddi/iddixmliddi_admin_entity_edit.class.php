<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiXmlIddi_Admin_Entity_Edit extends iddiXmlIddiNode {

    /**
     * Array of field definition objects to use when building the table
     * By default this will come from the entity definition and will include all fields
     * from the entity.
     * You can override by providing a comma seperated list of field names in
     * the @fields attributes
     * For more control override with iddi:admin-entity-table-field or a subclass of
     * @var array
     */
    var $fields = array();
    var $groups = array();
    var $entity_definition;
    var $row;

    function parse() {
        $this->processAVT();
        $this->entity = $this->attributes['ENTITY'];
        $this->entityid = $this->attributes['ID'];        
        
        $this->entity_definition = new iddiEntityDefinition($this->entity);


        $data = $this->get_data($this->entity, $this->entityid);
        $this->setDataSource($data);
        $this->row = $data->getFirstRow();
        $form = $this->prepare_form();
        $this->tab_container = new iddiXmlIddi_Admin_Entity_Edit_Tabs();
        $form->appendChild($this->tab_container);
        
        $allfields = $this->get_fields();
        $this->add_related();

        $this->appendChild($form);

        parent::parse();

        return $out;
    }

    function add_related(){
        $sql="SELECT distinct entityname,fieldname from `{PREFIX}sysentityfields` WHERE REPLACE(LOWER(lookup),' ','')='{$this->entity}'";        
        $rs=iddiMySql::query($sql);
        
        if($rs->hasData()){
            foreach($rs as $row){
                $group = $this->get_group($row->entityname.'s');
                $table = new iddiXmlIddi_Admin_Entity_Table();
                $table->attributes['ENTITY']=$row->entityname;
                $table->attributes['FILTER']=iddiMySql::tidyname($row->fieldname).'='.$this->entityid;
                $group->appendChild($table);
            }
        }
    }
    
    function prepare_form() {
        $this->form = new iddiHtml_Form();
        $this->form->attributes['DATA-ENTITY'] = $this->entity;
        $this->form->attributes['DATA-ENTITYID'] = $this->id;
        return $this->form;
    }

    function get_actions() {
        $fixed_front_fields = array(
            'save_action' => $this->save_action(),
            'lock_action' => $this->lock_action(),
            'delete_action' => $this->delete_action()
        );
    }

    function get_fields() {
        if(!$this->entity_definition->fielddefs['pagetitle']) $this->entity_definition->fielddefs['pagetitle']=$this->pagetitle_field();
        if(!$this->entity_definition->fielddefs['id']) $this->entity_definition->fielddefs['id']=$this->id_field();
        if(!$this->entity_definition->fielddefs['created']) $this->entity_definition->fielddefs['created']=$this->created_field();
        if(!$this->entity_definition->fielddefs['virtualfilename']) $this->entity_definition->fielddefs['virtualfilename']=$this->virtualfilename_field();

        $this->auto_set_fields();

        return $this->fields;
    }

    function auto_set_fields() {
        if ($this->attributes['FIELDS']) {
            $usefields = explode(',', $this->attributes['FIELDS']);
            
            foreach ($usefields as $field) {
                $this->append_field($field);
            }
        } else {
            foreach ($this->entity_definition->fielddefs as $field)
                $this->append_field($field);
        }
    }

    function append_field($fieldname) {
        if (is_string($fieldname)) {
            if (!isset($this->entity_definition->fielddefs[$fieldname]))
                return;
            $f = $this->create_field($this->entity_definition->fielddefs[$fieldname]);
        }elseif ($fieldname instanceof iddiEntityField) {
            $f = $this->create_field($fieldname);
        } else {
            throw new iddiException('Invalid Field', 'iddi.admin.entity_edit.append_field.invalid_input');
        }
        $f->set_form($this);
        $this->add_field($f);
    }

    /**
     * Adds a new field to the list of fields to render
     * @param iddiXmlIddi_Admin_Entity_Table_Field $field
     */
    function create_field(iddiEntityField $field) {
        $classname = 'iddiXmlIddi_Admin_Entity_Edit_Field_' . $field->type;
        if (!class_exists($classname))
            $classname = 'iddiXmlIddi_Admin_Entity_Edit_Field';
        $newfield = new $classname();
        $newfield->field = $field;

        $group_name = $field->group;
        if ($group_name == '') {
            $group_name = 'General';
            $field->group = ucfirst($group_name);
        }

        $group = $this->get_group($group_name);
        $group->appendChild($newfield);

        return $newfield;
    }

    function get_group($groupname) {
        $groupkey = iddiMySql::tidyname($groupname);
        if (!$this->groups[$groupkey]) {
            $group = new iddiXmlIddi_Admin_Entity_Edit_Tab();
            $group->caption = $groupname;
            $this->groups[$groupkey] = $group;
            $this->tab_container->appendChild($group);
        }
        return $this->groups[$groupkey];
    }

    /**
     * Adds a new field to the list of fields to render
     * @param iddiXmlIddi_Admin_Entity_Table_Field $field
     */
    function add_field(iddiXmlIddi_Admin_Entity_Edit_Field $field) {
        $this->fields[$field->field->fieldname] = $field;
    }

    /**
     * Removes the given field frm the fields to render
     * Can also pass in a string
     * If there are no matches then nowt happens
     * @param iddiEntityField $field
     */
    function remove_field($field) {
        if (is_string($field)) {
            unset($this->fields[$field]);
        } elseif (is_object($field) && $field instanceOf iddiEntityField) {
            unset($this->fields[$field->fieldname]);
        }
    }

    function id_field() {
        $id = new iddiEntityField();
        $id->caption = "ID";
        $id->fieldname = 'id';
        $id->is_key = true;
        $id->can_edit = false;
        $id->group='Properties';
        return $id;
    }

    function created_field() {
        $id = new iddiEntityField();
        $id->fieldname = 'created';
        $id->type = 'datetime';
        $id->caption = 'Created';
        $id->can_edit = true;
        $id->group='Properties';
        return $id;
    }
    
    function virtualfilename_field() {
        $id = new iddiEntityField();
        $id->fieldname = 'virtualfilename';
        $id->type = 'text';
        $id->caption = 'URL';
        $id->can_edit = true;
        $id->group='Properties';
        return $id;
    }    

    function pagetitle_field() {
        $pagetitle = new iddiEntityField();
        $id->fieldname = pagetitle;
        $pagetitle->caption = "Name";
        $pagetitle->fieldname = 'pagetitle';
        $pagetitle->group='General';
        return $pagetitle;
    }

    function edit_action() {
        $edit = new iddiEntityField();
        $edit->fieldname = 'save_action';
        $edit->is_action = true;
        $edit->action = 'iddi-save';
        $edit->type = 'action';
        $edit->icon = 'fa-save';
        return $edit;
    }

    function lock_action() {
        $lock = new iddiEntityField();
        $lock->fieldname = 'lock_action';
        $lock->is_action = true;
        $lock->action = 'iddi-delete-item';
        $lock->type = 'action';
        $lock->icon = 'fa-lock';
        return $lock;
    }

    function delete_action() {
        $delete = new iddiEntityField();
        $delete->fieldname = 'delete_action';
        $delete->is_action = true;
        $delete->action = 'iddi-delete-item';
        $delete->type = 'action';
        $delete->icon = 'fa-trash';
        return $delete;
    }

    function get_data($entity, $id) {
        $sql = 'SELECT * FROM iddi_sysfilenames f RIGHT JOIN {PREFIX}' . $entity . ' t ON f.id=t.id AND f.entityname="' . $entity . '" WHERE t.id = ' . $id;
        $d = iddiMySQL::query($sql);
        return $d;
    }

}
