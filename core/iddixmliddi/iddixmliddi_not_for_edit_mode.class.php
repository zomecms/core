<?php
    /**
    * iddiXmlIddi_Not_For_Edit_Mode Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_Not_For_Edit_Mode extends iddiXmlIddiNode{
        function output(){
            if(iddiRequest::getMode()!='edit') return parent::output();
        }
    }

   