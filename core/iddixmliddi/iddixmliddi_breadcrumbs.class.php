<?php
    /**
    * iddiXmlIddi_BreadCrumbs Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiXmlIddi_BreadCrumbs extends iddiXmlIddi_List_Items{
        function parse(){

            $this->processAVT();
            $this->preparse();
            $repeatable_loop=($this->repeatable)?$this->repeatable:$this;
            $repeatable_first=($this->repeatable_first)?$this->repeatable_first:$repeatable_loop;
            $repeatable_last=($this->repeatable_last)?$this->repeatable_last:$repeatable_loop;

            $current_ds=$this->getDataSource();
            $depth=0;
            while($current_ds && $depth<40){
              if($current_ds->parentid>0){
                if($current_ds->id>1){
                  if(substr($current_ds->pagetitle,0,1)!='[') $rs[]=$current_ds;
                }
                $pid=$current_ds->parentid;
                $current_ds=new iddiEntity();
                $current_ds->loadById('',$pid);
              }else{
                if(substr($current_ds->pagetitle,0,1)!='[') $rs[]=$current_ds;
                $current_ds=null;
              }
              $depth++;
            }
            //array_pop($rs);
            $rs=array_reverse($rs);
            $count=sizeof($rs);
            if ($rs)
            {
                $this->setDataSource($rs);
                $clone_first=$repeatable_first->clonenode(null,true);
                $clone_loop=$repeatable_loop->clonenode(null,true);
                $clone_last=$repeatable_last->clonenode(null,true);
                $pos=0;
                foreach($repeatable_first->children as $child) $repeatable_first->removeChild($child);
                foreach($repeatable_loop->children as $child) $repeatable_loop->removeChild($child);
                foreach($repeatable_last->children as $child) $repeatable_last->removeChild($child);
                foreach($rs as $record) {
                    //Load the item
                    $ent=$record;
                    //Select whilch clone to use
                    $clone=($pos==0)?$clone_first:(($pos==sizeof($rs)-1)?$clone_last:$clone_loop);
                    $repeatable=$repeatable_last;
                    if ($this->limiter==null || $pos >= $this->limiter->start){
                       if ($clone->children){
                            foreach($clone->children as $child) {
                                $newchild=$child->clonenode($repeatable,true);
                                $newchild->setDataSource($ent);
                                $newchild->_pos=$pos;
                                //$newchild->parse();
                                $newchild->processAVT();
                                if ($this->owner->getDataSource()->id==$record->id) $newchild->addClass('active');
                                if ($pos==0) $newchild->addClass('first');
                                if ($pos==$count-1) $newchild->addClass('last');
                                if ($ent->id==$selected) $newchild->setAttribute('selected','selected');
                                if ($ent->id==$checked) $newchild->setAttribute('checked','checked');
                            }
                        }
                    }
                    ++$pos;
                    if ($this->limiter!=null && $pos > $this->limiter->length+$this->limiter->start) break;
                }
                if($pos==0) foreach($this->children as $child) $this->removeChild($child);
                foreach($this->children as $child) $child->Parse();
            }else{
                foreach($this->children as $child) $this->removeChild($child);
            }
      }
    }
   