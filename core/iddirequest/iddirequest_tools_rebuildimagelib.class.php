<?php
    /**
    * iddiRequest_tools_rebuildimagelib Class file
    * @author J.Patchett - Tastic Multimedia
    * @package IDDI Core
    **/
    class iddiRequest_tools_rebuildimagelib{
      function getresponse(){
        $d=opendir(IDDI_IMAGES_PATH.'/originals');
        while($subdir=readdir($d)){
          if($subdir!='.' && $subdir!='..'){
            if(is_dir(IDDI_IMAGES_PATH.'/originals'.$subdir)){
              $this->process_dir($subdir);
            }
          }
        }
        return 'done';
      }

      function render(){
        return 'done';
      }

      function process_dir($dir){
        echo "<li>Processing Folder $dir";
        echo str_repeat("\n",255);
        flush();
        $d=opendir(IDDI_IMAGES_PATH.'/originals'.$dir);
        $storepath=IDDI_IMAGES_PATH.'/originals'.$dir.'/';
        $libarypath=IDDI_IMAGES_PATH.'/library/'.$dir.'/';
        @mkdir($libarypath);
        @chmod($libarypath,0777);
        while($filename=readdir($d)){
          if(is_file($storepath.$filename)){
        echo str_repeat("\n",255);
        flush();
            if(!file_exists($libarypath.$filename)){
                echo "<li>Processing $storepath$filename";
                $image=new iddiImage($storepath.$filename,95,95,'force');
                $image->save($libarypath.$filename,40);
                unset($image);
            }
          }
        }
      }
    }

    //---- Debugger / Monitor ----
