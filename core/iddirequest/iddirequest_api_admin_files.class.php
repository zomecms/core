<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiRequest_api_admin_files extends iddiApi_Result {
    var $sortkey='timestamp';
    var $sortdescending=true;

    function output() {
        $library = preg_replace('/[^a-z0-9_\-]/is', '', filter_input(INPUT_GET, 'library', FILTER_SANITIZE_STRING));
        $path = IDDI_IMAGES_PATH . DIRECTORY_SEPARATOR . 'originals' . DIRECTORY_SEPARATOR . $library;

        if ($library == '') {
            throw new iddiException('Missing parameter library', 'yendoi.api.files.missing_parameter.library');
        }

        if (strlen($library) > 50) {
            throw new iddiException('Suspicious parameter library', 'yendoi.api.files.missing_parameter.suspect');
        }

        if (strstr($library, '/') || strstr($library, '\\') || strstr($library, '..')) {
            throw new iddiException('Invalid library name', 'yendoi.api.files.invalid_library_path');
        }

        if (!is_dir($path)) {
            throw new iddiException('Invalid library name', 'yendoi.api.files.unknown_library_path');
        }
        $this->library = $library;
        $this->path = $path;
        $this->files = $this->get_files($path);
        parent::output();
    }

    function get_files($path) {
        $files = array();
        $d = opendir($path);
        while ($f = readdir($d)) {
            $filename = $path . DIRECTORY_SEPARATOR . $f;
            if ($f !== '.' && $f !== '..' && is_file($filename)) {
                $imageinfo = getimagesize($filename);
                $file = new stdClass();
                $file->name = $f;
                $file->size = filesize($filename);
                $file->date_created = date('Y-m-d', filectime($filename));
                $file->timestamp = filectime($filename);
                if ($imageinfo) {
                    $file->is_image = true;
                    $file->width = $imageinfo[0];
                    $file->height = $imageinfo[1];
                    $file->mime_type = $imageinfo['mime'];
                } else {
                    $file->is_image = false;
                }
                $files[] = $file;
            }
        }
        usort($files, array($this, "sorter"));        
        return $files;
    }
    
    function sorter($a, $b) {
        $key=$this->sortkey;
        return ($this->sortdescending) ? $a->$key < $b->$key : $a->$key > $b->$key;
    }

}
