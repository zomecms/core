<?php

/**
 * iddiRequest Class file
 * @author J.Patchett - Tastic Multimedia
 * @package IDDI Core
 * */
class iddiRequest extends iddiEvents {

    const BEFORE_GET_RESPONSE = 'BeforeGetResponse',
            GET_RESPONSE_BEFORE_USE_CACHED_RESPONSE = 'GetResponse_BeforeUseCachedResponse',
            AFTER_GET_RESPONSE_BEFORE_REPLY = 'AferGetResponse_BeforeReply';

    static $form;

    /**
     * @desc The current active response
     * @var iddiResponse
     */
    static $currentresponse;
    var $pageuri;
    var $language = 'en-gb';
    var $baselanguage = 'en-gb';
    var $starttime, $memory;
    var $is_compiling = false;
    var $isajax = false, $usecache = false;
    static $is_edit_mode = false;
    static $current;

    function __construct($ismain = false) {
        self::$current = $this;
        $this->starttime = microtime();
        $this->memory = memory_get_usage(true);

        //Reparse the request vars that will have been destroyed by rewrite
        $pagebits = explode('?', $_SERVER['REQUEST_URI'], 2);
        $this->pageuri = $pagebits[0];
        $pagevars = explode('&', $pagebits[1]);
        foreach ($pagevars as $pagevar) {
            $varbits = explode('=', $pagevar);
            $varname = $varbits[0];
            if ($varname != '') {
                $_REQUEST[$varname] = $varbits[1];
                $_GET[$varname] = $varbits[1];
            }
        }

        //Determine response type
        if ($_POST['ajax'] == 1 || $_REQUEST['ajax'] == 1)
            $this->isajax = true;
        
        self::$is_edit_mode = ($this->getMode()=='edit');
    }

    function __sleep() {
        unset($this->SESSION);
        return( array_keys(get_object_vars($this)) );
    }

    /**
     * @desc Gets the mode of the current request, will be either 'edit' or 'view'
     */
    function getMode() {
        //$m=($_REQUEST['mode'])?$_REQUEST['mode']:'view';
        $u = $_SESSION['user'];
        $m = ($u && substr_count($u->getValue('accesslevels'), 'admin') && !$_REQUEST['ajax']) ? 'edit' : 'view';
        $_SESSION['mode'] = $m;
        return $m;
    }

    static function getform() {
        if (!self::$form) {
            self::$form = new iddiform();
        }
        return self::$form;
    }

    /**
     * @desc Gets the response for the current request.
     */
    function getResponse() {
        iddiUser::startup();

        $e = $this->trigger(self::BEFORE_GET_RESPONSE);
        if (!$e->cancelled) {
            try {
                $page = new iddiPage();
                $page->LoadPage($this->pageuri);
                $this->template = $page->getTemplate();                
            } catch (iddiException $e) {
                switch ($e->code) {
                    case 'iddi.data.loadpagebyid.entitynotfound':
                    case 'iddi.data.loadpagebyvf.pagenotfound':
                        try {
                            $page = new iddiPage();
                            $page->LoadPage('/errors/404');
                            $this->template = $page->getTemplate();
                        } catch (iddiException $e) {
                            switch ($e->code) {
                                case 'iddi.data.loadpagebyid.entitynotfound':
                                case 'iddi.data.loadpagebyvf.pagenotfound':
                                    die('<h1>There was a problem</h1><p>Unfortunatly the page (' . $this->pageuri . ') you were looking for was not found.</p>');
                                    break;
                                default:
                                    echo $e;
                            }
                        }
                        break;
                    case 'iddi.data.loadpagebyid.entitydeleted':
                        header('HTTP/1.1 410 Gone', '410');
                        try {
                            $page = new iddiPage();
                            $page->LoadPage('/errors/410-Gone');
                            $this->template = $page->getTemplate();
                        } catch (iddiException $e) {
                            switch ($e->code) {
                                case 'iddi.data.loadpagebyid.entitynotfound':
                                case 'iddi.data.loadpagebyvf.pagenotfound':
                                    die('<h1>There was a problem</h1><p>Unfortunatly the page you were looking for is no longer available.</p>');
                                    break;
                                default:
                                    echo $e;
                            }
                        }
                        break;
                    default:
                        echo $e;
                }
            }
        }

        $e = $this->trigger(self::AFTER_GET_RESPONSE_BEFORE_REPLY);
        return $this->template->output();
    }

    function toXml($parent = null, $array = null) {
        if ($array == null)
            $array = $_REQUEST;
        if ($parent == null) {
            $doc = new iddiXmlDocument();
            $parent = new iddiXmlNode($doc, 'request', $doc, null);
        } else {
            $doc = $parent->owner;
        }

        foreach ($_REQUEST as $k => $v) {
            $n = new iddiXmlIddiNode($doc, $k, $parent, null);
            if (is_array($v)) {
                foreach ($v as $a) {
                    $this->toXml($doc, $a);
                }
            } else {
                $n->value = $v;
            }
        }
        return $doc;
    }

}
