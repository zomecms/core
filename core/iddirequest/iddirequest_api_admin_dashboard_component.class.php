<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiRequest_api_admin_dashboard_component extends Api_Result {

    function output() {
        try {
            $type = filter_input(INPUT_POST,'component_type',FILTER_SANITIZE_STRING);            
            
            if (!$_SESSION['script'][$type])
                throw new iddiException('Invalid component ' . $type, 'yendoi.api.dashboard_component.not_in_request');

            return iddiTemplate::render('dashboard_components/' . $type);
        } catch (Exception $e) {
            return 'Error';
        }
    }

    function get_stat($name) {
        $name = "get_$name";
        $data = iddiCache::get('STAT_' . $name);
        if ($data == null) {
            $data = $this->$name();
            //iddiCache::save('STAT_'.$name, $data,120);
        }
        return $data;
    }

    function get_total_sales() {
        $sql = 'SELECT sum(total_price) v from iddi_order WHERE balance_to_pay=0';
        $rs = iddiMySql::query($sql);
        $out->total = $rs->getFirstRow()->v;

        $sql = 'SELECT sum(total_price) v from iddi_order WHERE balance_to_pay=0 and created>=date_sub(now(),interval 30 day)';
        $rs = iddiMySql::query($sql);
        $out->this_month = intval($rs->getFirstRow()->v);

        $sql = 'SELECT sum(total_price) v from iddi_order WHERE balance_to_pay=0 and created>=date_sub(now(),interval 60 day) and created<date_sub(now(),interval 30 day)';
        $rs = iddiMySql::query($sql);
        $out->previous_month = intval($rs->getFirstRow()->v);

        $out->velocity = intval((($out->this_month - $out->previous_month) / $out->this_month) * 100);

        return $out;
    }

    function get_total_sales_volume() {
        $sql = 'SELECT sum(total_items) v from iddi_order WHERE balance_to_pay=0 and total_price>0';
        $rs = iddiMySql::query($sql);
        $out->total = $rs->getFirstRow()->v;

        $sql = 'SELECT sum(total_items) v from iddi_order WHERE balance_to_pay=0 and total_price>0 and created>=date_sub(now(),interval 30 day)';
        $rs = iddiMySql::query($sql);
        $out->this_month = intval($rs->getFirstRow()->v);

        $sql = 'SELECT sum(total_items) v from iddi_order WHERE balance_to_pay=0 and total_price>0 and created>=date_sub(now(),interval 60 day) and created<date_sub(now(),interval 30 day)';
        $rs = iddiMySql::query($sql);
        $out->previous_month = intval($rs->getFirstRow()->v);

        $out->velocity = intval((($out->this_month - $out->previous_month) / $out->this_month) * 100);

        return $out;
    }

    function get_total_orders() {
        $sql = 'SELECT count(id) v from iddi_order WHERE balance_to_pay=0 and total_price>0';
        $rs = iddiMySql::query($sql);
        $out->total = $rs->getFirstRow()->v;

        $sql = 'SELECT count(id) v from iddi_order WHERE balance_to_pay=0 and total_price>0 and created>=date_sub(now(),interval 30 day)';
        $rs = iddiMySql::query($sql);
        $out->this_month = intval($rs->getFirstRow()->v);

        $sql = 'SELECT count(id) v from iddi_order WHERE balance_to_pay=0 and total_price>0 and created>=date_sub(now(),interval 60 day) and created<date_sub(now(),interval 30 day)';
        $rs = iddiMySql::query($sql);
        $out->previous_month = intval($rs->getFirstRow()->v);

        $out->velocity = intval((($out->this_month - $out->previous_month) / $out->this_month) * 100);

        return $out;
    }

    function get_total_customers() {
        $sql = 'SELECT count(id) v from iddi_customer';
        $rs = iddiMySql::query($sql);
        $out->total = $rs->getFirstRow()->v;

        $sql = 'SELECT count(id) v from iddi_customer WHERE created>=date_sub(now(),interval 30 day)';
        $rs = iddiMySql::query($sql);
        $out->this_month = intval($rs->getFirstRow()->v);

        $sql = 'SELECT count(id) v from iddi_customer WHERE created>=date_sub(now(),interval 60 day) and created<date_sub(now(),interval 30 day)';
        $rs = iddiMySql::query($sql);
        $out->previous_month = intval($rs->getFirstRow()->v);

        $out->velocity = intval((($out->this_month - $out->previous_month) / $out->this_month) * 100);

        return $out;
    }

}
