<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class iddiRequest_api_logout extends Api_Result {

    function output() {
        session_destroy();
        if (class_exists('iddiCache')) iddiCache::flush_all();
        $this->success=true;
        parent::output();
    }

}
