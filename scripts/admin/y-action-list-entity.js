$('html').on('click', '*[data-action-name=y-action-list-entity]', function () {
    var entity_name = $(this).attr('data-y-entity');
    var page = parseInt($(this).attr('data-y-page'));
    var search = $(this).attr('data-y-search');
    if (isNaN(page) || page === 0)
        page = 1;
    if (usable(search) && search !== '') {
        yendoi.pushState('#/y-action-list-entity/' + entity_name + '/page:' + page + '/search:' + search, $(this).text());
    } else {
        yendoi.pushState('#/y-action-list-entity/' + entity_name + '/page:' + page, $(this).text());
    }
});

$(window).on('y-popstate', function (event, state) {
    if (state[0] === 'y-action-list-entity') {
        yendoi.switch_to_admin_mode();
        if (usable(state.search)) {
            yendoi.load('/api/admin/entity_table?entity=' + state[1] + '&ajax=1&page=' + state.page + '&quicksearch=' + state.search, function () {
                var search = JSON.parse(decodeURIComponent(state.search));
                console.log(search);
                $('input[data-y-role=quicksearch]').val(search.quicksearch);
                $('.y-search-bar input,.y-search-bar select').each(function () {
                    var val = search[$(this).attr('name')];
                    if (usable(val)) {
                        $(this).val(val);
                    }
                });
            });
        } else if (usable(state.page)) {
            yendoi.load('/api/admin/entity_table?entity=' + state[1] + '&ajax=1&page=' + state.page);
        } else {
            yendoi.load('/api/admin/entity_table?entity=' + state[1] + '&ajax=1&page=1');
        }
    }
});