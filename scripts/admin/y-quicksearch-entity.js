yendoi.entity_search=function(){
    var entity_name=$('.y-search-bar button').attr('data-entity-name');
    var quicksearch={};
    quicksearch.quicksearch=$('.y-search-bar input[data-y-role=quicksearch]').val();
    $('.y-search-bar select').each(function(){
        if($(this).val()!=='0'){
            quicksearch[$(this).attr('name')]=$(this).val();
        }
    });
    quicksearchtext=encodeURIComponent(JSON.stringify(quicksearch));
    yendoi.pushState('#/y-action-list-entity/'+entity_name+'/page:1/search:'+quicksearchtext,'Search Results');    
};

$('html').on('click', '*[data-action-name=y-quicksearch-entity]', function (e) {
    e.preventDefault();
    yendoi.entity_search();
});

$('html').on('change','.y-search-bar select',function(){
    yendoi.entity_search();
});