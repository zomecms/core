yendoi.help={};
yendoi.help.en={
    'View':'Turns off all editing. When in view mode you can navigate your website as normal. When you find the page you want to edit just click edit mode to re-enabled the editor',
    'Edit':'This will enable live editing direct on the current page. When in live edit mode certain website functions will not work, for example any links to other pages that you can edit will not work, thereby enabling you to edit the content. If you need to click a link please switch to view mode first to disable editing, once the page you want to edit has loaded you can go back to editing by clicking on edit again',
    'Admin':'Enter the back office view where you can edit all content on your website, manage orders, customers etc...',
    'Image Manager':'View your image library, create new libraries, upload new images, delete image and manage your image collection',
    'Library':'Use libraries to organise your files. Select a library from the list to change the current list of image, or click the menu button to the right for the option to create a new library, or upload files to the current library',
    'Create Library':'Create Library',
    'Upload Image':'Upload images and files to the currently selected library',
    'Create Library Confirm':'Creates the library and switches to the newly created library ready for you to add files to',
    'Upload Image Confirm':'Uploads the selected files and returns back to the library view. The files you uploaded will appear at the TOP of the library',
    'File Date':'This is in the internationally recognised format Year-Month-Day',
    'Delete image from library':'Permanantly deletes the image from the library, there is no undo, and if the image is used on any pages this could cause problems. Use with caution. Remember newest files are at the top of the image library',
    'Use this image in the current field':'Use this image in the current field'
};