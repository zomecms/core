$('html').on('click', '*[data-action-name=y-action-save]', function () {
    //Create a save progress banner
    var save_message = yendoi.banner.add_message('<i class="fa fa-circle-o-notch fa-spin"></i>Saving').setDuration(0);
    //Save the data
    $.ajax({
        type: "POST",
        url: '/api/admin/entity_save_changes',
        dataType: 'json',
        data: yendoi.changelog,
        success: function (data) {
            yendoi.banner.add_message('<i class="fa fa-check"></i>' + data.message);
            save_message.finish();            
            /*
            for (var dataitem in data.entity.fields) {
                var di = data.entity.fields[dataitem];
                $('*[name=' + dataitem + ']').val(di);
            }
            $('*[name=virtualfilename]').val(data.virtualfilename);
            $('*[name=entityid]').val(data.id);*/
            yendoi.changelog = {};
            yendoi.refresh_log_counter();
        }
    });
});

