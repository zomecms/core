$('html').on('click', '*[data-y-action=logout]', function () {
    yendoi.banner.add_message('<i class="fa fa-circle-o-notch fa-spin"></i>Logging Out').setDuration(0);
    $.ajax({
        type: "POST",
        url: '/api/logout',
        dataType: 'json',        
        success: function (data) {
            yendoi.banner.add_message('<i class="fa fa-check"></i>' + data.message);
            document.location='/';
        }
    });
});

