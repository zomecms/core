$('html').on('click', '*[data-action-name=y-action-delete]', function () {
    var entity_name = $(this).data('y-entity-name');
    var entity_id = $(this).data('y-entity-id');
    //Create a save progress banner
    if (yendoi.confirm(yendoi.lang.confirm_delete(entity_name, entity_id)))
    {
        var message = yendoi.banner.add_message(yendoi.common.loader + yendoi.lang.delete).setDuration(0);
        //Save the data
        $.ajax({
            type: "POST",
            url: yendoi.endpoints.delete_entity,
            dataType: 'json',
            data: {
                entity_name: entity_name,
                entity_id: entity_id
            },
            success: function (data) {
                if (data.success) {
                    yendoi.banner.add_message(yendoi.common.success + data.message);
                    message.finish();
                    yendoi.reload();
                } else {
                    yendoi.banner.add_message(yendoi.common.fail + data.message);
                    message.finish();
                }
            }
        });
    }
});

