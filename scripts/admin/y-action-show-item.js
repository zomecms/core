$('html').on('click','*[data-action-name=y-action-show-item]',function(){
    var entity_name=$(this).attr('data-y-entity-name');
    var entity_id=$(this).attr('data-y-entity-id');
    yendoi.pushState('#/y-action-show-item/'+entity_name+'/'+entity_id,$(this).attr('title'));
});

$(window).on('y-popstate', function (event,state) {         
    if(state[0] === 'y-action-show-item'){
        yendoi.switch_to_admin_mode();
        yendoi.load('/api/admin/entity_edit?e=' + state[1] + '&i=' + state[2]);        
    }    
});