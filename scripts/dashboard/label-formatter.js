
function labelFormatter(label, series) {
    return "<div style='white-space:nowrap' class='pie-label'>"
            + label + " - " + Math.round(series.percent) + "%</div>";
}
