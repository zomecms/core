yendoi.flotbar = function (id, data, axis_data) {
    var _self = this;
    this.data = data;
    this.id = id;
    this.axis_data = axis_data;

    $.plot($("#" + _self.id),
            [_self.data],
            {
                series: {
                    bars: {
                        show: true,
                        lineWidth: 0,
                        barWidth: 0.95
                    },
                    points: {
                        radius: 0,
                        show: true
                    },
                    shadowSize: 2
                },
                grid: {                    
                    hoverable: true,
                    clickable: true,
                    tickColor: "#d5d5d5",                    
                    borderWidth: 1,
                    color: '#d5d5d5'
                },
                colors: ["#1ab394", "#464f88"],
                xaxis: {
                    ticks: _self.axis_data,
                    tickLength: 4
                },
                yaxis: {
                    ticks: 4, min: 0                    
                },
                tooltip: true
            }
    );
}