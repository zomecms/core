yendoi.flotpie = function (id, data) {
    var _self = this;
    this.data = data;
    this.id = id;

    $.plot(
            $("#" + _self.id),
            _self.data, {
                series: {
                    pie: {
                        show: true,
                        innerRadius: 0.4,
                        label: {
                            show: true,    
                            radius: 1,
                            formatter: labelFormatter,
                        }
                    }
                },
                grid: {
                    hoverable: true
                },
                legend: {
                    show: false
                }
            });

    $("#" + _self.id).bind("plothover", function (event, pos, item) {
        if (item) {
            $("#" + _self.id).find('.pieLabel').eq(item.seriesIndex).css('opacity',1).siblings('.pieLabel').css('opacity',0.2);
        }
    });
}