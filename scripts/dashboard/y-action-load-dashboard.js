$('html').on('click','*[data-action-name=y-action-load-dashboard]',function(){
    yendoi.pushState('#/y-dashboard/main');
})

$(window).on('y-popstate', function (event,state) {         
    if(state[0] === 'y-dashboard'){
        yendoi.switch_to_admin_mode();
        yendoi.load('/api/admin/dashboard');
        yendoi.refresh_buttons($(''));
    }    
});