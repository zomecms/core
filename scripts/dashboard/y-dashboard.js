yendoi.dashboardComponents={};

yendoi.dashboardComponent=function(name,options){
    var _self=this;
    this.holder=null;
    this.name=name;
    this.options=options;
    this.options.component_type=name;
    this.refresh=function(){
        _self.holder.addClass('loading');
        $.post('/api/admin/dashboard_component',_self.options,function(data){
            _self.holder.removeClass('loading').html(data);
            $('.y-sparkline').sparkline();
        });
    }
    this.setHolder=function(holder){
        _self.holder=holder;
        _self.holder[0].compoment=_self;
    }
}

yendoi.dashboard={
    components:[],
    availableComponents:[],
    clear:function(){
        yendoi.dashboard.components=[];
    },
    addComponent:function(componentName,options){
        var component=new yendoi.dashboardComponent(componentName,options);
        yendoi.dashboard.components.push(component);        
    },
    drawComponent:function(component){
        var dashboard=$('#dashboard');
        var holder=$('div[data-holder-for=' + component.name + ']');        
        component.setHolder(holder);                
        component.refresh();
    },
    drawAll:function(){        
        for(a=0;a<yendoi.dashboard.components.length;a++){
            yendoi.dashboard.drawComponent(yendoi.dashboard.components[a]);
        }
    }
}