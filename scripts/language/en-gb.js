yendoi.lang={
    delete : "Delete"
};

yendoi.lang.confirm_delete=function(entity_name,entity_id){
    return 'Are you sure you want to delete ' + entity_name + ' #' + entity_id;
}