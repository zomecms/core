yendoi.editors.shared={
    current_editor:null,
    hover:function(target){
        
    },
    activate:function(target){
        yendoi.editors.shared.current_editor = target;
        $(yendoi.selectors.floating_toolbars).addClass('y-hide');
        $('*[contentEditable]').removeAttr('contentEditable');
        $('*[data-y-current-editor]').removeAttr('data-y-current-editor');
        $(yendoi.selectors.all_editors).css('outline','none');
        $(yendoi.selectors.yendoi_popupmenu).hide();     
        target.attr('data-y-current-editor','true');
    }
};