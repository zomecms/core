$(function () {
    $('html').on('click', yendoi.selectors.image_library_toggle, function () {
        if ($(yendoi.selectors.image_library).hasClass('y-open')) {
            yendoi.image_library.close();
        } else {
            yendoi.image_library.open();
        }
    });
});

yendoi.editors.link_editor = function () {
    var _self = this;
    _self.ui = $('#yendoi-link-editor');
    _self.editlink = null;

    _self.open = function (e) {
        $('.y-active').removeClass('y-active');
        $('#yendoi-link-editor').addClass('y-open').siblings().removeClass('y-open');
        e.preventDefault();
    };

    _self.close = function () {
        $('#yendoi-link-editor').removeClass('y-open');
    };

    _self.link_edit = function (e) {
        var link = $(yendoi.html_editor.current_range.commonAncestorContainer).parents('a');
        $('#yendoi-htmleditor-toolbar i[y-cmd="editlink"]').remove();
        if (link.length > 0) {
            var edit_button = $('<i class="fa fa-link" y-cmd="editlink"><span>Edit Link</span></i>');
            $('#yendoi-htmleditor-toolbar').append(edit_button);
            _self.show_internal();
            _self.browse_internal(link.attr('href'));
            _self.editlink = link;
        } else {            
            _self.editlink = null;
        }
    };

    _self.pick_type = function () {
        $('#link-type').show();
        $('#link-editor-internal').hide();
        $('#link-editor-external').hide();
        $('#link-editor-file').hide();
    };

    _self.show_internal = function () {
        $('#link-type').hide();
        $('#link-editor-internal').show();
        $('#link-editor-external').hide();
        $('#link-editor-file').hide();

        if (yendoi.html_editor.selection_has_content()) {
            $('#link-editor-external input[name=link-editor-internal-text]').hide();
        } else {
            $('#link-editor-external input[name=link-editor-internal-text]').show();
        }

        if ($('#internal-links li').length === 0) {
            _self.browse_internal('/');
        }
    };

    _self.browse_internal = function (path) {
        var _path = path;

        $.getJSON(yendoi.endpoints.list_urls, {path: path}, function (data) {
            var items = data.pages.length;
            if (items > 0) {
                $('#internal-links li').remove();
                if (path !== '') {
                    var uplink = $('<li class="up" title="Go back to previous folder"><i class="fa fa-chevron-up"/>Back Up</li>');
                    uplink.attr('y-cmd', 'link-internal-browse');
                    uplink.attr('data-path', data.up);
                    $('#internal-links').append(uplink);
                }
                for (var a = 0; a < items; a++) {
                    var item = data.pages[a];
                    var link = $('<li></li>');
                    var expand = $('<i class="fa fa-chevron-right"/>');
                    var details = $('<span/>');
                    expand.attr('y-cmd', 'link-internal-browse');
                    expand.attr('data-path', item.url);
                    details.text(item.title);
                    details.attr('title', item.url);
                    details.attr('y-cmd', 'link-internal-select');
                    link.append(expand);
                    link.append(details);
                    $('#internal-links').append(link);
                }
                $('#link-editor-internal .y-confirm').hide();
            } else {
                $('#internal-links li[title="' + path + '"] i').remove();
            }
        });
    };

    _self.browse_internal_click = function (e) {
        var path = $(this).attr('data-path');
        _self.browse_internal(path);
    };

    _self.link_internal_select = function (e) {
        $(this).parent().addClass('selected').siblings().removeClass('selected');
        $('#link-editor-internal .y-confirm').show();
    };

    _self.create_internal_link = function () {
        var url = $('#link-editor-internal li.selected span').attr('title');

        if (_self.editlink === null) {
            var link = yendoi.html_editor.wrap_selection('a');
            $(link).attr('href', url).removeAttr('target');
        }
        else {
            _self.editlink.attr('href', url).removeAttr('target');
        }
        //yendoi.html_editor.pasteHtmlAtCaret();

        _self.ui.trigger(yendoi.events.link_editor_set_link, {url: url});
        _self.close();
    };


    _self.show_external = function () {
        $('#link-type').hide();
        $('#link-editor-internal').hide();
        $('#link-editor-external').show();
        $('#link-editor-file').hide();

        if (yendoi.html_editor.selection_has_content()) {
            $('#link-editor-external input[name=link-editor-external-text]').hide();
        } else {
            $('#link-editor-external input[name=link-editor-external-text]').show();
        }
    };

    _self.create_external_link = function () {
        var url = $('#link-editor-external input[name=link-editor-external-url]').val();
        
        if (_self.editlink === null) {
            var link = yendoi.html_editor.wrap_selection('a');
            $(link).attr('href', url).attr('target', '_blank');
        }
        else {
            _self.editlink.attr('href', url).attr('target', '_blank');
        }

        //yendoi.html_editor.pasteHtmlAtCaret();

        _self.ui.trigger(yendoi.events.link_editor_set_link, {url: url});
        _self.close();
    };

    _self.show_file = function () {
        $('#link-type').hide();
        $('#link-editor-internal').hide();
        $('#link-editor-external').hide();
        $('#link-editor-file').show();

        if (yendoi.html_editor.selection_has_content()) {
            $('#link-editor-external input[name=link-editor-external-text]').hide();
        } else {
            $('#link-editor-external input[name=link-editor-external-text]').show();
        }

    };

    _self.pick_image = function (target_editor) {
        _self.target_editor = target_editor;
        _self.open();
        $('#library-item-detail *[y-cmd="image-insert-img"]').hide();
        $('#library-item-detail *[y-cmd="image-use-img"]').show();
    };

    _self.update_content = function () {
        _self.target_editor.update_image(_self._current_library, _self.library_item.name);
    };

    _self.upload_file_progress = function (data) {
        console.log(data);
    };

    _self.upload_file = function () {
        var text = $('#link-editor-external input[name=link-editor-external-text]').val();
        var formData = new FormData($('#y-upload-file-form')[0]);
        formData.append('library', _self._current_library);
        $.ajax({
            url: '/api/admin/file_upload',
            type: 'POST',
            xhr: function () {  // custom xhr
                myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) { // if upload property exists
                    myXhr.upload.addEventListener('progress', _self.upload_file_progress, false); // progressbar
                }
                return myXhr;
            },
            //Ajax events
            success: completeHandler = function (data) {
                data = JSON.parse(data);
                if (data.success) {
                    _self.ui.trigger(yendoi.events.link_editor_set_link, {text: text, url: data.url});
                    _self.close();
                } else {
                    alert(data.error.message);
                }
            },
            error: errorHandler = function () {
                alert("Unknown upload error");
            },
            // Form data
            data: formData,
            //Options to tell JQuery not to process data or worry about content-type
            cache: false,
            contentType: false,
            processData: false
        }, 'json');
    };

    yendoi.events.link_editor_set_link = 'link_editor_set_link';

    $('html').on('click', 'i[y-cmd="editlink"]', _self.open);
    $('html').on('click', 'i[y-cmd="link-editor-internal"]', _self.show_internal);
    $('html').on('click', 'i[y-cmd="link-editor-external"]', _self.show_external);
    $('html').on('click', 'i[y-cmd="link-editor-file"]', _self.show_file);
    $('html').on('click', 'i[y-cmd="link-type"]', _self.pick_type);
    $('html').on('click', 'i[y-cmd="close-link-editor"]', _self.close);
    $('html').on('click', 'i[y-cmd="link-internal-confirm"]', _self.create_internal_link);
    $('html').on('click', 'i[y-cmd="link-external-confirm"]', _self.create_external_link);
    $('html').on('click', 'i[y-cmd="link-file-confirm"]', _self.upload_file);
    $('html').on('click', '*[y-cmd="link-internal-browse"]', _self.browse_internal_click);
    $('html').on('click', 'span[y-cmd="link-internal-select"]', _self.link_internal_select);

    $('html').on('y-selection-changed', '*[data-y-fieldtype="html"]', _self.link_edit);

    _self.pick_type();
};

$(document).ready(function () {
    yendoi.link_manager = new yendoi.editors.link_editor();
});
