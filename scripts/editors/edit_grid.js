
yendoi.grid_edit = {
    keys: {
        up: 38,
        down: 40,
        left: 37,
        right: 39,
        f2: 113,
        esc: 27,
        del: 46,
        enter: 13
    },
    setup: function () {
        $('html').on('click', '#iddi-admin-main-area td', function (e) {
            $('td.focus').removeClass('focus');
            $(this).addClass('focus');
        });
        $('html').on('keyup', function (e) {
            var currentfocus = $('td.focus');
            if (currentfocus.length === 0) {
                currentfocus = $('td[data-editable]:first');
            }
            newfocus = [];
            switch (e.keyCode) {
                case yendoi.grid_edit.keys.up:
                    e.preventDefault();
                    newfocus = currentfocus.parent().prev('tr').find('td').eq(currentfocus.index());
                    break;
                case yendoi.grid_edit.keys.down:
                    e.preventDefault();
                    newfocus = currentfocus.parent().next('tr').find('td').eq(currentfocus.index());
                    break;
                case yendoi.grid_edit.keys.left:
                    e.preventDefault();
                    newfocus = currentfocus.prev('td');
                    break;
                case yendoi.grid_edit.keys.right:
                    e.preventDefault();
                    newfocus = currentfocus.next('td');
                    break;
                case yendoi.grid_edit.keys.f2:
                    e.preventDefault();
                    if (currentfocus.attr('data-editable') !== '') {
                        currentfocus.attr('contenteditable', true).focus();
                    }
                    break;
                case yendoi.grid_edit.keys.esc:
                case yendoi.grid_edit.keys.enter:
                    e.preventDefault();
                    currentfocus.attr('contenteditable', false).focus();
                    break;
            }
            if (newfocus.length > 0) {
                $('td.focus').removeClass('focus');
                newfocus.addClass('focus').focus();
            }

        });
    }
};

