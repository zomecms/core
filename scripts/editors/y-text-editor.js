yendoi.editors.text_editor = function () {
    var _self = this;
    var _selector = yendoi.selectors.text_editors;
    var _current = {
        node : null,
        max_length : 100
    };
    
    _self.initiate_editor = function (e) {
        if (yendoi.viewmode === yendoi.viewmodes.EDIT) {
            e.stopPropagation();
            _current.node = $(this);
            yendoi.editors.shared.activate(_current.node);
            if(_current.node.attr('data-y-maxlength')){
                _current.node.max_length = _current.node.attr('data-y-maxlength');
            } else{
                _current.node.max_length = 100;
            }
            _current.node.attr('contentEditable', true);      
            _current.node.css('outline', 'solid 1px #444');
        }
    };
    
    _self.lost_focus = function (e) {
        if (yendoi.viewmode === yendoi.viewmodes.EDIT) {
            //e.stopPropagation();
            //$(this).removeAttr('contentEditable');
        }
    };
    
    _self.gain_focus = function (e) {
        if (yendoi.viewmode == yendoi.viewmodes.EDIT) {
            e.stopPropagation();
            if (!$(this).hasClass('yendoi-edit-active')) {
                $('.yendoi-edit-active').removeClass('yendoi-edit-active');
                $('*[data-y-entityid]').css('outline', '');
                $('#yendoi-popupmenu').hide();

                $(this).addClass('yendoi-edit-active')
                        .attr('contentEditable', true)
                        .css('outline', 'solid 1px #444');

            }
        }
    };
    
    _self.key_press = function (e) {
        if (yendoi.viewmode === yendoi.viewmodes.EDIT) {
            switch (e.keyCode) {
                case yendoi.grid_edit.keys.enter:
                    e.preventDefault();
                    return false;                    
            }
        }
    };
    
    _self.key_up = function (e) {
        if (yendoi.viewmode === yendoi.viewmodes.EDIT) {
            _self.save_changes();
        }
    };    
    
    _self.paste = function (e) {
        if (yendoi.viewmode === yendoi.viewmodes.EDIT) {
            e.preventDefault();

            var replacementText = e.originalEvent.clipboardData.getData('text/plain').replace(/(\r\n|\n|\r)/gm, ''),
                    sel,
                    range;
            
            if(replacementText.length > _current.node.max_length) replacementText=replacementText.substr(0,_current.node.max_length-1);
            
            if (window.getSelection) {
                sel = window.getSelection();
                if (sel.rangeCount) {
                    range = sel.getRangeAt(0);
                    range.deleteContents();
                    range.insertNode(document.createTextNode(replacementText));
                }
            } else if (document.selection && document.selection.createRange) {
                range = document.selection.createRange();
                range.text = replacementText;
            }
            
            _self.save_changes();
        }
    };
    
    _self.save_changes=function(){
        if(usable(_current.node)){
            var entityid = _current.node.attr('data-y-entityid');
            var entitytype = _current.node.attr('data-y-entityname');
            var fieldname = _current.node.attr('data-y-fieldname');
            var value = _current.node.text();
            yendoi.logchange(entityid, entitytype, fieldname, value);
        }
    };
    
    _self.ignore=function(e){
        e.preventDefault();
        e.stopPropagation();
    };
    
    $('body').on('mousedown', _selector, _self.initiate_editor)
            .on('focus', _selector, _self.initiate_editor)
            .on('blur', _selector, _self.lost_focus)
            .on('yendoi.click', _selector, _self.gain_focus)
            .on('keydown', _selector, _self.key_press)
            .on('keyup', _selector, _self.key_up)            
            .on('paste', _selector, _self.paste);
};

$(document).ready(function(){
    new yendoi.editors.text_editor();
});