/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



yendoi.editors.image_picker = function () {
    var _self=this,
        _node;
    
    _self.select_image = function () {
        yendoi.image_library.pick_image(_self);
    };

    _self.update_image = function (library, image) {
        var imagesrc = '/api/image?height=40&width=40&outmode=image&scalemode=force&library=' + library + '&file=' + image;
        _node.attr('src', imagesrc);
        _node.attr('rawsrc', '/iddi-project/images/originals/'+library+'/'+image);
        _self.save_changes();        
    };

    _self.save_changes = function () {
        if (usable(_node)) {
            var entityid = _node.attr('data-y-entityid'),
                    entitytype = _node.attr('data-y-entityname'),
                    fieldname = _node.attr('data-y-fieldname'),
                    value = _node.attr('rawsrc');
            yendoi.logchange(entityid, entitytype, fieldname, value);
        }
    };

    $('html').on('click', '*[data-action-name=y-image-chooser]', function (e) {
        e.preventDefault();
        _node = $(this).parent().find('img');
        _self.select_image();
    });
};

$(document).ready(function () {
    new yendoi.editors.image_picker();
});