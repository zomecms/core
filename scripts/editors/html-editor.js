/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$('html').on('blur', '.y-html-editor .htmleditor', function (e) {
    $(this).parents('fieldset').find('textarea').val($(this).html());
});

$('html').on('blur', '.y-html-editor textarea', function (e) {
    $(this).parents('fieldset').find('.htmleditor').html($(this).val());
});