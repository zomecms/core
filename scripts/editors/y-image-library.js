$(function () {
    $('html').on('click', yendoi.selectors.image_library_toggle, function () {
        if ($(yendoi.selectors.image_library).hasClass('y-open')) {
            yendoi.image_library.close();
        } else {
            yendoi.image_library.open();
        }
    });
});

yendoi.editors.image_library = function () {
    var _self = this,
            _current_library = 'General';

    _self.open = function () {
        $(yendoi.selectors.image_library).addClass('y-open');
        $(yendoi.selectors.image_library_toggle).addClass('y-active');
        $('#library-item-detail *[y-cmd="image-insert-img"]').hide();
        $('#library-item-detail *[y-cmd="image-use-img"]').hide();
        _self.refresh_libraries();
        _self.get_library(_current_library);
        _self.show_library();
    };

    _self.close = function () {
        $(yendoi.selectors.image_library).removeClass('y-open');
        $(yendoi.selectors.image_library_toggle).removeClass('y-active');
    };

    _self.refresh_libraries = function () {
        var $library_list = $('#library-selector');
        $library_list.find('option').remove();
        $.getJSON('/api/admin/libraries', function (data) {
            for (var a = 0; a < data.libraries.length; a++) {
                var library = data.libraries[a];
                var $library = $('<option/>').attr('value', library.name).text(library.caption);
                $library_list.append($library);
            }
            $('#library-selector').val(_self._current_library);
        });
    };

    _self.get_library = function (library_name) {
        _self.show_library();
        _self._current_library = library_name;
        var $library_items = $('#library-items');
        $('#library-selector').val(library_name);
        $library_items.find('li').remove();
        var used_width = 0;
        var current_images = [];
        $.getJSON('/api/admin/files', {library: library_name}, function (data) {
            for (var a = 0; a < data.files.length; a++) {
                var library_item = data.files[a];
                if (library_item.is_image) {
                    var imagesrc = '/api/image?height=70&width=160&outmode=image&scalemode=force&library=' + library_name + '&file=' + library_item.name;
                    var $library_item = $('<li/>');
                    var $library_image = $('<img/>').attr('data-y-filename', library_item.name).attr('src', imagesrc);
                    $library_item.append($library_image);
                    $library_items.append($library_item);
                    $library_image[0].item_ratio = library_item.width / library_item.height;
                    $library_image[0].item_width = (70 * $library_image[0].item_ratio) + 6;
                    $library_image[0].data = library_item;
                    used_width += $library_image[0].item_width;
                    if (used_width >= 300) {
                        var height = ((300 / (used_width - $library_image[0].item_width)) * 70);
                        for (var b = 0; b < current_images.length; b++) {
                            var item = current_images[b];
                            var width = height * item[0].item_ratio;
                            var imagesrc2 = '/api/image?height=' + height + '&width=' + width + '&outmode=image&scalemode=force&library=' + library_name + '&file=' + item[0].data.name;
                            item.css('height', (height) + 'px').css('width', (width) + 'px').attr('src', imagesrc2);
                        }
                        used_width = $library_image[0].item_width;
                        current_images = [];
                    }
                    current_images.push($library_image);
                }
            }
            //Last row
            var final_height = ((300 / (used_width)) * 70);
            for (var bb = 0; bb < current_images.length; bb++) {
                var itemb = current_images[bb];
                var width2 = final_height * itemb[0].item_ratio;
                var imagesrc3 = '/api/image?height=' + final_height + '&width=' + width2 + '&outmode=image&scalemode=force&library=' + library_name + '&file=' + itemb[0].data.name;
                itemb.css('height', (final_height) + 'px').attr('src', imagesrc3);
            }
        });
    };

    _self.show_library = function () {
        $('#library-item-detail').hide();
        $('#library-upload').hide();
        $('#library-create').hide();
        $('#library-library').show();
    };

    _self.show_item = function () {
        _self.library_item = $(this)[0].data;

        var imagesrc = '/api/image?height=420&width=320&outmode=image&scalemode=force&library=' + _self._current_library + '&file=' + _self.library_item.name;
        $('#library-item-detail img').attr('src', imagesrc);

        $('#library-item-detail *[data-y-bind=filename]').text(_self.library_item.name);
        $('#library-item-detail *[data-y-bind=dateuploaded]').text(_self.library_item.date_created);
        $('#library-item-detail *[data-y-bind=filesize]').text(_self.library_item.size / 1024 + ' Kb');

        $('#library-library').hide();
        $('#library-upload').hide();
        $('#library-create').hide();
        $('#library-item-detail').show();
    };

    _self.show_create_library = function () {
        $('#library-library').hide();
        $('#library-upload').hide();
        $('#library-item-detail').hide();
        $('#library-create').show();
    };

    _self.show_upload_file = function () {
        $('#library-library').hide();
        $('#library-create').hide();
        $('#library-item-detail').hide();
        $('#library-upload').show();
    };

    _self.pick_image = function (target_editor) {
        _self.target_editor = target_editor;
        _self.open();
        $('#library-item-detail *[y-cmd="image-insert-img"]').hide();
        $('#library-item-detail *[y-cmd="image-use-img"]').show();
    };

    _self.update_image_field = function () {
        _self.target_editor.update_image(_self._current_library, _self.library_item.name);
    };

    _self.delete_file = function () {
        if (confirm('Are you sure you want to delete this image. There is no undo')) {
            $.getJSON('/api/admin/file_delete', {library: _self._current_library, file: _self.library_item.name}, function (data) {
                if (data.success) {
                    _self.get_library(_self._current_library);
                } else {
                    alert('Image deletion failed');
                }
            });
        }
    };

    _self.create_library = function () {
        var library_name = $('input[name=y-library-name]').val().trim();
        if (library_name === '') {
            alert('Please enter a library name');
        } else {
            $.getJSON('/api/admin/library_create', {name: library_name}, function (data) {
                if (data.success) {
                    _self.refresh_libraries();
                    _self.get_library(data.library_created);
                } else {
                    alert(data.error.message);
                }
            });
        }
    };

    _self.upload_file_progress=function(data){
        console.log(data);
    };

    _self.upload_file = function () {
        var formData = new FormData($('#y-upload-file-form')[0]);
        formData.append('library',_self._current_library);
        $.ajax({
            url: '/api/admin/file_upload',
            type: 'POST',
            xhr: function () {  // custom xhr
                myXhr = $.ajaxSettings.xhr();
                if (myXhr.upload) { // if upload property exists
                    myXhr.upload.addEventListener('progress', _self.upload_file_progress, false); // progressbar
                }
                return myXhr;
            },
            //Ajax events
            success: completeHandler = function (data) {
                data = JSON.parse(data);
                if (data.success) {
                    _self.get_library(_self._current_library);
                } else {
                    alert(data.error.message);
                }                                
            },
            error: errorHandler = function () {
                alert("Unknown upload error");
            },
            // Form data
            data: formData,
            //Options to tell JQuery not to process data or worry about content-type
            cache: false,
            contentType: false,
            processData: false
        }, 'json');
    };


    _self.refresh_libraries();

    $('html').on('change', '#library-selector', function () {
        var library_name = $(this).val();
        _self.get_library(library_name);
        _self.show_library();
    });

    $('html').on('click', '#library-items img', _self.show_item);
    $('html').on('click', 'i[y-cmd="image-library-create"]', _self.show_create_library);
    $('html').on('click', 'i[y-cmd="image-library-upload"]', _self.show_upload_file);
    $('html').on('click', 'i[y-cmd="image-library-home"]', _self.show_library);
    $('html').on('click', 'i[y-cmd="image-use-img"]', _self.update_image_field);
    $('html').on('click', 'i[y-cmd="image-delete-img"]', _self.delete_file);
    $('html').on('click', 'i[y-cmd="image-library-do-create"]', _self.create_library);
    $('html').on('click', 'i[y-cmd="image-library-do-upload"]', _self.upload_file);
    $('html').on('click', 'i[y-cmd="close-image-manager"]', _self.close);       
};

$(document).ready(function () {
    yendoi.image_library = new yendoi.editors.image_library();
});