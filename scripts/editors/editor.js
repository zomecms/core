/* 
 * Appends the editor html to the end of the document, and the css
 */
yendoi.load_cms = function () {
    $.get('/api/admin/editor', function (data) {
        $('html').trigger('y-ready');
        $('html').append(data);
        yendoi.switch_to_edit_mode();
    });
    $('head').append($('<link rel="stylesheet" type="text/css" />').attr('href', '/iddi/iddi.css'));
};

yendoi.changelog = {};

yendoi.logchange = function (entityid, entitytype, fieldname, value) {
    this.entityid = entityid;
    this.entitytype = entitytype;
    this.fieldname = fieldname;
    this.value = value;

    $('html').trigger('y-value-change', this);

    var key = entitytype + '/' + entityid + '/' + fieldname;

    yendoi.changelog[key] = value;
    yendoi.refresh_log_counter();

};

yendoi.trackchange = function () {
    console.log('j');
    var entityid = $(this).attr('data-y-entityid');
    var entitytype = $(this).attr('data-y-entityname');
    var fieldname = $(this).attr('data-y-fieldname');
    var value = $(this).is('input,select,textarea') ? $(this).val() : $(this).html();
    yendoi.logchange(entityid, entitytype, fieldname, value);
};

yendoi.refresh_log_counter = function () {
    var count = Object.keys(yendoi.changelog).length;
    if (count > 0) {
        $('.yendoi-changelog-count').text(count).show();
    } else {
        $('.yendoi-changelog-count').text(count).hide();
    }
};

$('html').on('y-ready', function () {
    $('html').on('keyup', '*[data-y-entityid][data-y-entityname][data-y-fieldname]', yendoi.trackchange);
    $('html').on('change', '*[data-y-entityid][data-y-entityname][data-y-fieldname]', yendoi.trackchange);

    $('body').on('mousemove', '*[data-y-entityid]', function (e) {
        if (yendoi.viewmode === yendoi.viewmodes.EDIT && !$(this).is('[data-y-current-editor]')) {
            $('#yendoi-popupmenu #y-fieldname').text('Click to edit ' + $(this).attr('data-y-fieldname'));
            var x = $(this).offset().left - 1,
                    y = $(this).offset().top - 100;
            $('*[data-y-entityid]').css('outline', '');
            $(this).css('outline', 'dotted 1px #444');
            $('#yendoi-popupmenu').show().css({'top': y + 'px', 'left': x + 'px'});
        }
    });


    $('body').on('mouseleave', '*[data-y-entityid]', function (e) {
        $(yendoi.selectors.yendoi_popupmenu).hide();
    });

    $('body').on('click', '*[data-y-entityid]', function (e) {
        if (yendoi.viewmode === yendoi.viewmodes.EDIT) {
            e.preventDefault();
            e.stopPropagation();
            $(yendoi.selectors.floating_toolbars).addClass('y-hide');
            $(this).trigger(yendoi.events.yendoi_click);
        }
    });

    $('body').on('click', 'a', function (e) {
        if (yendoi.viewmode === yendoi.viewmodes.EDIT) {
            e.preventDefault();
            e.stopPropagation();
            $(this).trigger(yendoi.events.yendoi_click);
        }
    });

    $('body').on('click', 'i', function () {
        $('.y-clicked').removeClass('y-clicked');
        $(this).addClass('y-clicked');
    });

    $('body').on('click', function (e) {
        $(yendoi.selectors.floating_toolbars).addClass('y-hide');
        $('*[data-y-entityid]').css('outline', '');
        $(yendoi.selectors.yendoi_popupmenu).hide();
    });

    $(yendoi.selectors.floating_toolbars).addClass('y-hide');
});