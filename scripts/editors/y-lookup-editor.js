$('body').on('click', '*[data-y-fieldtype="lookup"]', function (e) {
    if (yendoi.viewmode == yendoi.viewmodes.EDIT && !$(this).hasClass('yendoi-edit-active')) {
        $('.yendoi-edit-active').removeClass('yendoi-edit-active');
        $('*[data-y-entityid]').css('outline', '');
        $('#yendoi-popupmenu').hide();
        
        $(this).addClass('yendoi-edit-active')
            .css('outline', 'solid 4px #444');
    
        var lookup = $(this).attr('data-y-lookupentity');
        alert('choose for '+lookup);
    }
});
