yendoi.editors.html_editor = function () {
    var _self = this,
            _selector = yendoi.selectors.html_editors,
            _toolbar = yendoi.selectors.html_editor_toolbar,
            _current = {
                node: null,
                max_length: 100
            },
    _valid_elements = {
        top_level: ['address', 'audio', 'blockquote', 'canvas', 'div', 'dl', 'form', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'hr', 'ol', 'p', 'pre', 'table', 'ul', 'video'],
        nestable: ['ul', 'ol', 'li', 'dl', 'dd', 'dt'],
        un_nest: ['td', 'tr', 'th', 'tbody', 'thead', 'address', 'audio', 'blockquote', 'canvas', 'div', 'dl', 'form', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'hr', 'ol', 'u', 'p', 'pre', 'table', 'ul', 'video', 'b', 'big', 'i', 'small', 'tt', 'abbr', 'acronym', 'cite', 'code', 'dfn', 'em', 'kbd', 'strong', 'samp', 'var', 'a', 'bdo', 'br', 'img', 'map', 'object', 'q', 'script', 'span', 'sub', 'sup', 'button', 'input', 'label', 'select', 'textarea'],
        remove: ['object', 'script']
    };

    _self.current_range = null;
    _self.selectiontimeout = null;

    _self.initiate_editor = function (e) {
        if (yendoi.viewmode === yendoi.viewmodes.EDIT) {
            e.stopPropagation();
            _current.node = $(this);
            yendoi.editors.shared.activate(_current.node);
            _current.node.max_length = _current.node.attr('data-y-maxlength') || 100;
            _current.node.attr('contentEditable', true);
            _current.node.css('outline', 'solid 1px #444');

            var x = _current.node.offset().left - 1,
                    y = _current.node.offset().top - 100;
            $(_toolbar).removeClass('y-hide').css({'top': y + 'px', 'left': x + 'px'});
            $(_toolbar).find('#y-fieldname').text(_current.node.attr('data-y-fieldname'));
            _self.clean();
        }
    };

    _self.lost_focus = function (e) {
        if (yendoi.viewmode === yendoi.viewmodes.EDIT) {
            //e.stopPropagation();
            //$(this).removeAttr('contentEditable');
            //$(_toolbar).hide();
        }
    };

    _self.gain_focus = function (e) {
        if (yendoi.viewmode == yendoi.viewmodes.EDIT) {
            e.stopPropagation();
            _self.set_selection(null);
            if (!$(this).hasClass('yendoi-edit-active')) {
                $('.yendoi-edit-active').removeClass('yendoi-edit-active');
                $('*[data-y-entityid]').css('outline', '');
                $('#yendoi-popupmenu').addClass('y-hide');

                $(this).addClass('yendoi-edit-active')
                        .css('outline', 'solid 1px #444');

            }
        }
    };

    _self.key_up = function (e) {
        if (yendoi.viewmode === yendoi.viewmodes.EDIT) {
            _self.save_changes();
        }
    };

    _self.paste = function (e) {
        if (yendoi.viewmode === yendoi.viewmodes.EDIT) {
            e.preventDefault();
            var replacementText = e.originalEvent.clipboardData.getData('text/html').clean_html();
            if (replacementText.trim() === '') {
                replacementText = e.originalEvent.clipboardData.getData('text/plain').replace(/(\r\n|\n|\r)/gm, '<br/>');
                if (replacementText.indexOf("\t")) {
                    replacementText = '<pre>' + replacementText + '</pre>';
                }
            }
            _self.set_range(null);
            _self.pasteHtmlAtCaret(replacementText);
            _self.save_changes();
        }
    };

    _self.pasteHtmlAtCaret = function (html, selectPastedContent) {
        var sel,
                range;

        range = _self.current_range;
        range.deleteContents();

        // Range.createContextualFragment() would be useful here but is
        // only relatively recently standardized and is not supported in
        // some browsers (IE9, for one)
        var el = document.createElement("div");
        el.innerHTML = html;
        var frag = document.createDocumentFragment(), node, lastNode;
        while ((node = el.firstChild)) {
            lastNode = frag.appendChild(node);
        }
        var firstNode = frag.firstChild;
        range.insertNode(frag);

        // Preserve the selection
        if (lastNode) {
            range = range.cloneRange();
            range.setStartAfter(lastNode);
            if (selectPastedContent) {
                range.setStartBefore(firstNode);
            } else {
                range.collapse(true);
            }
            sel.removeAllRanges();
            sel.addRange(range);
            _self.set_selection(range);
        }

        _self.clean();
    };

    _self.selection_end = function (e) {
        sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
            range = sel.getRangeAt(0);
        }

        _self.set_selection(range);
    };

    _self.set_selection = function (range) {
        _self.current_range = range;
        _current.node.trigger('y-selection-changed');

        if (_self.selection_has_content()) {
            var bounds = range.getBoundingClientRect();
            var width = $('#html_selection_toolbar').width();
            //Create toolbar at location    
            var x = bounds.left + ((bounds.right - bounds.left) / 2) - (width / 2),
                    y = window.scrollY + bounds.top - 49 - 49 - 9;
            
            if (x < 0){
                $('#html_selection_toolbar .fa-caret-down').css('margin-left',(x-10)+'px');
                x = 0;
            }else{
                $('#html_selection_toolbar .fa-caret-down').css('margin-left','-10px');
            }
                
            $('#html_selection_toolbar').removeClass('y-hide').css({'top': y + 'px', 'left': x + 'px'});
            $('#selection-marker-left').addClass('visible').css({'top': (window.scrollY + bounds.top - 4 - 49) + 'px', 'left': bounds.left + 'px'});
            $('#selection-marker-right').addClass('visible').css({'top': (window.scrollY + bounds.bottom - 19 - 49) + 'px', 'left': bounds.right + 'px'});
        } else {
            $('#html_selection_toolbar').addClass('y-hide');
            $('#selection-marker-left, #selection-marker-right').removeClass('visible');
        }
                
    };

    _self.selection_has_content = function () {
        return _self.current_range !== null && _self.current_range.endOffset > _self.current_range.startOffset;
    };

    _self.selection_changed = function (e) {
        if (_self.selectiontimeout !== null) {
            clearTimeout(_self.selectiontimeout);
        }
        _self.selectiontimeout = setTimeout(function (e) {
            _self.selection_end(e);
        }, 250);
    };

    _self.htmlinsertimage = function () {
        alert('insertimage');
    };

    _self.htmlinsertfile = function () {
        alert('insertfile');
    };

    _self.bold = function (e) {
        _self.wrap_selection('strong');
    };

    _self.italic = function () {
        _self.wrap_selection('em');
    };

    _self.underline = function () {
        _self.wrap_selection('u');
    };

    _self.blockquote = function () {
        _self.wrap_selection('blockquote');
    };

    _self.p = function () {
        _self.wrap_selection('p');
    };

    _self.h1 = function () {
        _self.wrap_selection('h1');
    };

    _self.h2 = function () {
        _self.wrap_selection('h2');
    };

    _self.htmlol = function () {
        document.execCommand('insertOrderedList');
    };

    _self.htmlul = function () {
        document.execCommand('insertUnorderedList');
    };

    _self.clearstyling = function () {
        var range2 = _self.current_range,
                text = range2.cloneContents();

        range2.deleteContents();
        _self.pasteHtmlAtCaret(text.textContent);
        _self.clean();
    };

    _self.wrap_selection = function (nodeName) {
        var range2 = _self.current_range,
                nextup = $(range.commonAncestorContainer).parents(_selector + ' ' + nodeName).eq(0),
                newNode;

        if (nextup.length === 0) {
            newNode = document.createElement(nodeName);
            range2.surroundContents(newNode);
        } else {
            newNode = document.createElement('DUMMY');
            range2.surroundContents(newNode);
            var e = nextup[0].outerHTML;
            e = e.replace(/<dummy(.*?)>/ig, '</' + nodeName + '>').replace(/<\/dummy>/ig, '<' + nodeName + '>');
            nextup[0].outerHTML = e;
        }

        _self.clean();

        return newNode;
    };

    /**
     * This cleans up the markup in the current editor after any changes
     * Key is to prevent any nodes of the same time being nested inside each other
     * Although _self would be potentially valid in a html document, it should not
     * be done inside content managed content
     * Run through top level elements in _self editor
     * All top level must be block level, if any are not we'll wrap them in a p tag
     * Once we start iterating through any block level elements that have made their way
     * inside the element will be rolled back up to the top level
     * 
     * Note : _self makes use of inner/outerHTML. Would be much better to stick with dom
     * maniplation, but proving tricky
     */
    _self.clean = function () {
        //Only do _self if there is no text selected, otherwise we destroy that
        var range = _self.current_range;

        if (range === null || range.startContainer !== range.endContainer || range.startOffset !== range.endOffset) {
            return;
        }

        _current.node.contents().each(function () {
            var node_name = this.nodeName,
                    a,
                    node;

            if (_valid_elements.top_level.indexOf(node_name) === -1) {
                //Not valid so wrap in a p
                $(this).wrapAll('<p></p>');
            }

            //Unwrap all nested un-nestables
            for (a in _valid_elements.un_nest) {
                node = _valid_elements.un_nest[a];
                _current.node.find(node + ' ' + node).contents().unwrap();
                //_self.$target.find(node+'+'+node).each(function(){$(_self).html(($(_self).prev().html()+' '+$(_self).html())); $(_self).prev().remove()});
            }

            //Ensure all block levels are at the top
            var all_top = _valid_elements.top_level.join(',');
            for (a in _valid_elements.top_level) {
                node = _valid_elements.top_level[a];
                _current.node.find(node + ' ' + node).contents().unwrap();
                _current.node.find(all_top).find(node).each(_self.unwrap_not_self);
            }

        });
        //UNWRAP ALL STYING- Chrome puts them in and does bad things to them!
        _current.node.find('span:not([class])').contents().unwrap();
        _current.node.find('*').removeAttr('style');

        //Remove empty nodes
        _current.node.find('*:empty:not(br)').remove();

        //Regroup any text nodes otherwise creating subsequent ranges gets nasty
        _current.node[0].normalize();
    };

    _self.unwrap_not_self = function () {
        if ($(this).parent() !== _current.node) {
            $(this).unwrap();
        }
    };

    _self.save_changes = function () {
        if (usable(_current.node)) {
            var entityid = _current.node.attr('data-y-entityid'),
                    entitytype = _current.node.attr('data-y-entityname'),
                    fieldname = _current.node.attr('data-y-fieldname'),
                    value = _current.node.text();
            yendoi.logchange(entityid, entitytype, fieldname, value);
        }
    };

    _toolbar_command = function (cmd) {
        return yendoi.selectors.html_selection_toolbar + ' i[y-cmd=' + cmd + ']';
    };

    _self.ignore = function (e) {
        e.preventDefault();
        e.stopPropagation();
    };

    $('body').on('mousedown', _selector, _self.initiate_editor)
            .on('mouseup', _selector, _self.selection_changed)
            .on('focus', _selector, _self.initiate_editor)
            .on('blur', _selector, _self.lost_focus)
            .on('yendoi.click', _selector, _self.gain_focus)
            .on('keyup', _selector, _self.key_up)
            .on('paste', _selector, _self.paste);

    $(document).on('mousedown', _toolbar_command('htmlbold'), _self.bold)
            .on('mousedown', _toolbar_command('htmlitalic'), _self.italic)
            .on('mousedown', _toolbar_command('htmlunderline'), _self.underline)
            .on('mousedown', _toolbar_command('htmlol'), _self.htmlol)
            .on('mousedown', _toolbar_command('htmlul'), _self.htmlul)
            .on('mousedown', _toolbar_command('htmlnormal'), _self.p)
            .on('mousedown', _toolbar_command('htmlheading'), _self.h1)
            .on('mousedown', _toolbar_command('htmlsubheading'), _self.h2)
            .on('mousedown', _toolbar_command('htmlblockquote'), _self.blockquote)
            .on('mousedown', _toolbar_command('clearstyling'), _self.clearstyling);

    //document.addEventListener('selectionchange', _self.selection_changed);

};

$(document).ready(function () {
    yendoi.html_editor = new yendoi.editors.html_editor();
});