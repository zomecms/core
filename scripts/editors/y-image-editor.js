yendoi.editors.image_editor = function () {
    var _self = this,
            _selector = yendoi.selectors.image_editors,
            _toolbar = yendoi.selectors.image_editor_toolbar,
            _current = {
                node: null,
                max_length: 100
            };

    _self.initiate_editor = function (e) {
        if (yendoi.viewmode === yendoi.viewmodes.EDIT) {
            e.stopPropagation();
            _current.node = $(this);
            yendoi.editors.shared.activate(_current.node);
            _current.node.max_length = _current.node.attr('data-y-maxlength') || 100;
            _current.node.css('outline', 'solid 1px #444');

            var x = _current.node.offset().left - 1,
                    y = _current.node.offset().top - 100;
            $(_toolbar).removeClass('y-hide').css({'top': y + 'px', 'left': x + 'px'});
            $(_toolbar).find('#y-fieldname').text(_current.node.attr('data-y-fieldname'));
        }
    };

    _self.lost_focus = function (e) {
        if (yendoi.viewmode === yendoi.viewmodes.EDIT) {
            //e.stopPropagation();
            //$(this).removeAttr('contentEditable');
            //$(_toolbar).hide();
        }
    };

    _self.save_changes = function () {
        if (usable(_current.node)) {
            var entityid = _current.node.attr('data-y-entityid'),
                    entitytype = _current.node.attr('data-y-entityname'),
                    fieldname = _current.node.attr('data-y-fieldname'),
                    value = _current.node.attr('rawsrc');
            yendoi.logchange(entityid, entitytype, fieldname, value);
        }
    };

    _self.select_image = function () {
        yendoi.image_library.pick_image(_self);
    };

    _self.update_image = function (library, image) {
        var width = _current.node.attr('data-y-resize-width'),
                height = _current.node.attr('data-y-resize-height'),
                scalemode = _current.node.attr('data-y-resize-scalemode');

        var imagesrc = '/api/image?height=' + height + '&width=' + width + '&outmode=image&scalemode=' + scalemode + '&library=' + library + '&file=' + image;
        _current.node.attr('src', imagesrc);
        _current.node.attr('rawsrc', '/iddi-project/images/originals/'+library+'/'+image);
        _self.save_changes();
    };

    _toolbar_command = function (cmd) {
        return _toolbar + ' i[y-cmd=' + cmd + ']';
    };

    _self.ignore = function (e) {
        e.preventDefault();
        e.stopPropagation();
    };

    $('body').on('blur', _selector, _self.lost_focus)
            .on('yendoi.click', _selector, _self.initiate_editor);

    $('html').on('mousedown', _toolbar_command('imageedit-select'), _self.select_image)
            .on('mousedown', _toolbar_command('imageedit-remove'), _self.ignore)
            .on('mousedown', _toolbar_command('imageedit-edit'), _self.ignore);

};

$(document).ready(function () {
    new yendoi.editors.image_editor();
});