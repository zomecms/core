function usable(obj,undefined){
    try{
        return !(typeof obj === typeof undefined || obj === null);
    }catch(e){
        return false;
    }
}