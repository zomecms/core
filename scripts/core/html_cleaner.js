String.prototype.clean_html = function () {
    return this.replace(/<(.*?) (.*?)>/gm, '<$1>')
            .replace(/\n\n/g, "<br />")
            .replace(/<div>/gm, '<p>')
            .replace(/<\/div>/gm, '</p>')
            .replace(/<span>/gm, '')
            .replace(/<\/div>/gm, '')
            .replace(/<html>/gm, '')
            .replace(/<\/html>/gm, '')
            .replace(/<body>/gm, '')
            .replace(/<\/body>/gm, '')
            .replace(/<script>.*?<\/script>/gm, '')
            .replace(/<iframe>.*?<\/iframe>/gm, '')
            .replace(/<p><p>(.*?)<\/p><\/p>/gm, '<p>$1</p>')
            .replace(/<!--.*?-->/g, '').toString();
};
