/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$('html').on('keyup', '*[data-y-entityname][data-y-entityid]', function (e) {
    var entityName = $(this).attr('data-y-entityname');
    var entityId = $(this).attr('data-y-entityid');
    var fieldName = $(this).attr('data-y-fieldname');
    var val = $(this).is('input,select,textarea') ? $(this).val() : $(this).html();
    var $_self=$(this);
    $('*[data-y-entityname='+entityName+'][data-y-entityid='+entityId+'][data-y-fieldname='+fieldName+']').each(function(){
        if($(this)[0] != $_self[0]){
            if($(this).is('input,select,textarea')){
                $(this).val(val);
            }else{
                $(this).html(val);
            }
        }
    });
});