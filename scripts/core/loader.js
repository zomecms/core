yendoi.load=function(url,callback){
    $('#iddi-quicklinks').addClass('y-preplinks').removeClass('y-nolinks');
    var _loader = yendoi.banner.wipe_message('<i class="fa fa-circle-o-notch fa-spin"></i>Loading').setDuration(0);
    $('#iddi-admin-main-area').html('').load(url,function(){
        _loader.finish();
        if(usable(callback)) callback(data);
    });
};

yendoi.reload=function(){
    var state = yendoi.unpopurl(document.location.hash);
    $(window).trigger('y-popstate', state);
};