$(window).on('popstate', function (event) {
    var state = yendoi.unpopurl(document.location.hash);
    $(window).trigger('y-popstate', state);
    $('#y-tooltip').hide();
});

yendoi.pushState = function (url, pageTitle) {
    history.pushState({url: url}, pageTitle, url);
    $(window).trigger('y-popstate', yendoi.unpopurl(url));
    $('#y-tooltip').hide();
};

yendoi.unpopurl = function (url) {
    if (typeof url == 'undefined')
        return;
    if (url.substr(0, 1) == '#')
        url = url.substr(1);
    if (url.substr(0, 1) == '/')
        url = url.substr(1);
    var parts = url.split('/');
    var state = {};
    for (var a = 0; a < parts.length; a++) {
        var part = parts[a];
        if (part.indexOf(':') > -1) {
            var partParts = part.split(':', 2);
            state[partParts[0]] = partParts[1];
        } else {
            state[a] = part;
        }
    }
    return state;
};

$(document).ready(function () {
    setTimeout(function () {
        var state = yendoi.unpopurl(document.location.hash);
        $(window).trigger('y-popstate', state);
    }, 1000);
});
