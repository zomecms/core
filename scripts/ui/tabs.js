$('html').on('click', '.tabs li', function () {
    var index = $(this).index();
    $(this).closest('.tabs').find('> div').eq(index).addClass('active').siblings().removeClass('active');
    $(this).addClass('active').siblings().removeClass('active');
});