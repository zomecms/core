/**
 * Admin Mode
 */

yendoi.viewmodes = {
    VIEW: 0,
    EDIT: 1,
    ADMIN: 2
};

yendoi.viewmode = yendoi.viewmodes.EDIT;

yendoi.change_mode = function (mode) {
    if (yendoi.viewmode === mode)
        return;
    yendoi.viewmode = mode;
    $(document).trigger('yendoi_mode_change', yendoi.viewmode);
    $('#iddi-help-floater').hide();
};

yendoi.switch_to_view_mode = function () {
    $('#iddi-cmd-viewmode').addClass('y-active').siblings().removeClass('y-active');
    $('#iddi-admin-area').hide();
    $('body a[href-stash]').each(function () {
        $(this).attr('href', $(this).attr('href-stash')).removeAttr('href-stash');
        $(this).attr('target', '_top');
    });

    $('html').removeClass('yendoi-active').attr('yendoi-mode', 'view');
    $('div[data-y-mode="edit"]').hide();
    $('div[data-y-mode="admin"]').hide();
    $('div[data-y-mode="view"]').show();

    $('*[data-y-entityid]').css('outline', '');

    yendoi.change_mode(yendoi.viewmodes.VIEW);
    yendoi.refresh_buttons($('#yendoi-view-mode-buttons'));    
};

yendoi.switch_to_edit_mode = function () {
    $('html').addClass('yendoi-active').attr('yendoi-mode', 'edit');
    $('div[data-y-mode="edit"]').show();
    $('div[data-y-mode="admin"]').hide();
    $('div[data-y-mode="view"]').hide();

    $('#iddi-cmd-editmode').addClass('y-active').siblings().removeClass('y-active');
    $('#iddi-admin-area').hide();
    $('body a[href]').each(function () {
        $(this).attr('href-stash', $(this).attr('href')).removeAttr('href');
    });
    yendoi.change_mode(yendoi.viewmodes.EDIT);
    yendoi.refresh_buttons($('#yendoi-edit-mode-buttons'));    
};

yendoi.switch_to_admin_mode = function () {
    $('html').addClass('yendoi-active').attr('yendoi-mode', 'admin');
    $('div[data-y-mode="edit"]').hide();
    $('div[data-y-mode="admin"]').show();
    $('div[data-y-mode="view"]').hide();

    $('#iddi-cmd-adminmode').addClass('y-active').siblings().removeClass('y-active');
    $('#iddi-admin-area').show();
    if ($('#iddi-admin-main-area').html() === '')
        $('#iddi-admin-main-area').html('').load('/api/admin/dashboard');
    yendoi.change_mode(yendoi.viewmodes.ADMIN);
    yendoi.refresh_buttons($('#buttons'));    
};

$('html').on('click', '#iddi-cmd-adminmode', yendoi.switch_to_admin_mode);
$('html').on('click', '#iddi-cmd-editmode', yendoi.switch_to_edit_mode);
$('html').on('click', '#iddi-cmd-viewmode', yendoi.switch_to_view_mode);

$(document).ready(function(){
    yendoi.switch_to_edit_mode();
});