$(document).ready(function () {
    $('html').on('mouseover', '*[title]', function (e) {
        var title = $(this).attr('title');
        var content = yendoi.help.en[title] || title;
        if (content !== '') {
            $('#y-tooltip p').html(content);
            var x = $(this).offset().left - 1,
                    y = $(this).offset().top + $(this).height() - 30;
            $('#y-tooltip').show().css({'top': y + 'px', 'left': x + 'px'});
            
            $('#y-tooltip').addClass('y-tooltip-arrow-top-left').removeClass('y-tooltip-arrow-top-right');
            
            if(300 + x > $(window).width()){
                $('#y-tooltip').show().css({'left': (x - 300) + 'px'});
                $('#y-tooltip').removeClass('y-tooltip-arrow-top-left').addClass('y-tooltip-arrow-top-right');
            }
            
        } else {
            $('#y-tooltip').hide();
        }
    });
    $('html').on('mouseout', '*[title]', function (e) {
        $('#y-tooltip').hide();
    });
});
