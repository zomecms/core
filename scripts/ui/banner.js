yendoi.banner = {
    queue: [],
    ui: null,
    wipe_message: function (message) {
        yendoi.banner.queue=[];
        return yendoi.banner.add_message(message);
    },
    add_message: function (message) {
        if (yendoi.banner.ui === null)
            yendoi.banner.ui = $('#y-banner');
        var banner = new yendoi.banner.banner('<div>' + message + '</div>');
        yendoi.banner.queue.push(banner);
        if (yendoi.banner.queue.length == 1) {
            yendoi.banner.next_banner();
        }
        return banner;
    },    
    next_banner: function () {
        if (yendoi.banner.queue.length > 0) {
            var banner = yendoi.banner.queue[0];
            banner.reveal();
        } else {
            $('#y-banner').removeClass('y-open');
        }
    },
    banner: function (message) {
        var _duration = 5;
        var _hideTimer = null;
        var _self = this;
        this.reveal = function () {
            if (yendoi.banner.ui.hasClass('y-open')) {
                yendoi.banner.ui.addClass('y-next-message').removeClass('y-prep-message');
                setTimeout(function () {
                    yendoi.banner.ui.html(message).addClass('y-prep-message').removeClass('y-next-message');
                }, 250);
                setTimeout(function () {
                    yendoi.banner.ui.removeClass('y-prep-message');
                }, 300);
            } else {
                yendoi.banner.ui.html(message).addClass('y-open');
            }
            if (_duration > 0) {
                _hideTimer = setTimeout(_self.finish, _duration * 1000);
            }
            return this;
        };
        this.finish = function () {
            yendoi.banner.queue.shift();
            if (_hideTimer !== null)
                clearTimeout(_hideTimer);
            yendoi.banner.next_banner();
            return this;
        };
        this.setDuration = function (duration) {
            _duration = duration;
            if (_hideTimer !== null)
                clearTimeout(_hideTimer);
            if (_duration > 0) {
                _hideTimer = setTimeout(_self.finish, _duration * 1000);
            }
            return this;
        };
    }
};