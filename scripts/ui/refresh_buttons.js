
yendoi.refresh_buttons = function (source_area) {
    source_area.hide();
    $('#iddi-quicklinks').addClass('y-preplinks').removeClass('y-nolinks');
    setTimeout(function () {
        $('#iddi-quicklinks').removeClass('y-preplinks').addClass('y-nolinks').html('').append(source_area.find('i').clone());
        setTimeout(function () {
            $('#iddi-quicklinks').removeClass('y-nolinks').removeClass('y-preplinks');
        }, 200);    
    }, 250);
};