This error occurs if the CMS has been unable to successfully load any valid config files.

To resolve this check the following:

* Have you called iddi::AddProject at least once and passed in a valid project path?
* Does your project folder contain a config folder?
* Does your config folder contain at least iddiconfig.xml?
* Is the XML in the config file valid?

You should also check that at least one of your config files has a <site> section and at least one config file has at least one <databases> section.